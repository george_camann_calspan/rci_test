/*
 * rci_server.c
 *
 *  Created on: Jul 25, 2016
 *      Author: paul
 */
/*-- INCLUDES: ------------------------------------------------------------*/
#include "../src/rpi_msg_handler.h"
#include "cal_types.h"

#include "cal_constants.h"
#include "cal_platform.h"
#include "cal_time.h"
#include "cal_crc32.h"
#include "cal_swap.h"

#include <sys/socket.h>
#include <stdio.h>

#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

/*-- DEFINES: -------------------------------------------------------------*/

/*-- TYPEDEFS: ------------------------------------------------------------*/

/*-- CONSTANT DEFINITIONS: ------------------------------------------------*/

#define SLOW_FREQ           4 /* standard message frequency - 0.32 seconds for 1/50 second SNS DT */
#define ASM_FREQ            0 /* asm message frequency for smooth use of joystick - same as SNS DT */
#define MRM_CONTINUOUS      255
#define MRM_STOP            0

/*-- LOCAL FUNCTION DECLARATIONS: -----------------------------------------*/

static BOOL rpi_running = FALSE;
static BOOL rpi_stop = FALSE;

/* ip address to use, AF_INET for default */
static u_long rpi_inet_addr = AF_INET;

int                 sFd;           /* socket file descriptor */

/*-- FUNCTION DEFINITIONS: ------------------------------------------------*/

void print_hex(void *p,size_t size)
{
    unsigned char *ptr = (unsigned char*)p;
    unsigned int i = 0;
    for ( i = 0; i < size; i++ )
    {
        if( (i > 0)
         && (i % 16 == 0) )
        {
            printf("\n");
        }

        if( i % 16 == 0 )
        {
            printf("%04x: ", i);
        }

        if( (i % 8 == 0) && (i % 16 != 0) )
        {
            printf( "  " );
        }

        printf("%02x ",*(ptr++));
    }
    printf("\n");
}

/* call this function, if necessary, to set the IP address of the
 * interface to listen on, if the default address is no suffecient.
 */
STATUS rpi_set_ip(const char* ip_str)
{
    rpi_inet_addr = inet_addr( (char*)ip_str );

    return OK;
}

void rpi_shutdown()
{
    rpi_stop = TRUE;
    while(rpi_running)
    {
        cal_sleep(s2ct(1.0));
    }
}

#define LOCAL_IP_LEN (32)



CAL_TASK_ENTRY_RETURN_T  stop_task( void* pofp_state_in )
{
    printf( "**********************************************\n");
    printf( "*          PRESS 'c' <enter> TO EXIT         *\n");
    printf( "**********************************************\n");
    while( 'c' != getchar() )
    {
        cal_sleep( s2ct(0.1) );
    }

    printf( "Exitting....\n");
    rpi_shutdown();

    return(CAL_TASK_ENTRY_DEFAULT_RETURN_VAL);
}

/* sdts_network_server
 *
 * starts sdts network server.
 *
 * \param ip_str - ip address of interface to use, or NULL for default
 */
int main(int argc, char* argv[])
{
    int port, msg_id, msg_version;
    uint32_t dest_IP;
    if(argc < 5)
    {
        printf("Usage: IP, PORT, ID, VERSION\n");
        port = 1405;
        msg_id = 5;
        msg_version = 0;
    }
    else
    {
        port = atoi(argv[2]);
        msg_id = atoi(argv[3]);
        msg_version = atoi(argv[4]);
    }
    struct sockaddr_in  client_addr;   /* client's socket address */
    struct sockaddr_in  server_addr_resp;   /* server's socket address */
    unsigned int        sock_addr_size;/* size of socket address structure */
    int                 on;            /* bool for setsockopt */
    SNS_MSG             msg;

    on = 1;

    rpi_stop = FALSE;

    /* Initialize CRC32 for checksums */
    crc32_init();

    /* Set up the local address */

    sock_addr_size = sizeof (struct sockaddr_in);
    bzero ((char *) &client_addr, sock_addr_size);
    client_addr.sin_family = rpi_inet_addr;
#ifdef __vxworks
    client_addr.sin_len = (u_char) sock_addr_size;
#endif
    client_addr.sin_port = htons (RPI_PORT_NUM);
    client_addr.sin_addr.s_addr = htonl (INADDR_ANY);

    /* Create a UDP-based socket */

    if ((sFd = socket (AF_INET, SOCK_DGRAM, 0)) == ERROR)
    {
        perror ("socket");
        return (ERROR);
    }

    if (setsockopt(sFd, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on)) < 0)
    {
        perror ("sockopt");
        close (sFd);
        return (ERROR);
    }

    /* Bind socket to local address */

    if (bind (sFd, (struct sockaddr *) &client_addr, sock_addr_size) == ERROR)
    {
        perror ("bind");
        close (sFd);
        return (ERROR);
    }

    /* Send Message Request Messages */




    CAL_TASK_ID_T task_id;

    cal_task_start(&task_id,
                "task_stop",
                CAL_TASK_DEFAULT_PRIORITY,
                0,
                CAL_TASK_DEFAULT_STACK_SIZE,
                stop_task,
                (void*)NULL);

    /* Accept new connect requests and spawn tasks to process them */
    rpi_running = TRUE;

    while( TRUE != rpi_stop )
    {
        memset( &msg, '\0', sizeof(SNS_MSG) );

        if (recvfrom (sFd, (char *) &msg, sizeof(SNS_MSG), 0,
            (struct sockaddr *) &server_addr_resp, &sock_addr_size) == ERROR)
        {
            perror ("recvfrom");
            close (sFd);
            return (ERROR);
        }

        //printf( "=======================================================\n);
        printf( "Message Received : ");
		printf( "msg_id = %i, ", msg.header.msg_id);
		printf( "msg_size = %i, ", cal_ntohs(msg.header.msg_size));
		printf( "time = %f\n", ct2s(cal_gettime()));
#if 0
		printf( "=======================================================\n");
        print_hex(&msg,cal_ntohs(msg.header.msg_size));
        printf( "=======================================================\n");
#endif
    }
    rpi_running = FALSE;

    /* Stop Message Request Messages */
    printf( "Stopping messages... \n");



	/* Wait a little bit so user can see that the messages stopped */
	cal_sleep( s2ct(1.0) );

	printf( "Goodbye\n");

    return( 0 );
}
