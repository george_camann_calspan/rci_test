
#include "../src/rpi_msg_handler.h"
#include "../src/sns_msg_handler.h"

#include "cal_constants.h"
#include "cal_time.h"
#include "cal_platform.h"
#include "cal_swap.h"
#include "rtw.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/* Model data indexes */
#define L3_MODEL_INDEX_ALPHA                29
#define L3_MODEL_INDEX_PITCH                 8
#define L3_MODEL_INDEX_ROLL                  7
#define L3_MODEL_INDEX_ALTITUDE             63
#define L3_MODEL_INDEX_TRUE_AIRSPEED        28
#define L3_MODEL_INDEX_CALIBRATED_AIRSPEED  51
#define L3_MODEL_INDEX_EQUIV_AIRSPEED       53
#define L3_MODEL_INDEX_MACH                 54
#define L3_MODEL_INDEX_ALTITUDE_RATE        68
#define L3_MODEL_INDEX_PLATFORM_AZIMUTH      9
#define L3_MODEL_INDEX_LATITUDE            112
#define L3_MODEL_INDEX_LONGITUDE           113
#define L3_MODEL_INDEX_E_NORTH_VEL         115
#define L3_MODEL_INDEX_E_EAST_VEL          116
#define L3_MODEL_INDEX_E_DOWN_VEL          118
#define L3_MODEL_INDEX_FUEL_WEIGHT          96
#define L3_MODEL_INDEX_NX_PILOT             22
#define L3_MODEL_INDEX_NY_PILOT             23
#define L3_MODEL_INDEX_NZ_PILOT             24
#define L3_MODEL_INDEX_BETA                 30
#define L3_MODEL_INDEX_RADAR_ALTITUDE       64

/* Feel data indexes */
#define L3_FEEL_INDEX_CS_PITCH_FORCE         0
#define L3_FEEL_INDEX_CS_PITCH_POSITION      1
#define L3_FEEL_INDEX_CS_ROLL_FORCE          2
#define L3_FEEL_INDEX_CS_ROLL_POSITION       3
#define L3_FEEL_INDEX_WC_PITCH_FORCE         4
#define L3_FEEL_INDEX_WC_PITCH_POSITION      5
#define L3_FEEL_INDEX_WC_ROLL_FORCE          6
#define L3_FEEL_INDEX_WC_ROLL_POSITION       7
#define L3_FEEL_INDEX_SS_PITCH_FORCE         8
#define L3_FEEL_INDEX_SS_PITCH_POSITION      9
#define L3_FEEL_INDEX_SS_ROLL_FORCE         10
#define L3_FEEL_INDEX_SS_ROLL_POSITION      11
#define L3_FEEL_INDEX_RP_FORCE              12
#define L3_FEEL_INDEX_RP_POSITION           13

#define L3_DI_INDEX_CS_WC_TRIM_NOSE_UP             17
#define L3_DI_INDEX_CS_WC_TRIM_NOSE_DOWN           18
#define L3_DI_INDEX_CS_WC_TRIM_RIGHT_WING_DOWN     19
#define L3_DI_INDEX_CS_WC_TRIM_LEFT_WING_DOWN      20
#define L3_DI_INDEX_SS_TRIM_NOSE_UP                21
#define L3_DI_INDEX_SS_TRIM_NOSE_DOWN              22
#define L3_DI_INDEX_SS_TRIM_RIGHT_WING_DOWN        23
#define L3_DI_INDEX_SS_TRIM_LEFT_WING_DOWN         24
#define L3_DI_INDEX_YAW_TRIM_NOSE_RIGHT            25
#define L3_DI_INDEX_YAW_TRIM_NOSE_LEFT             26

/* Discrete data indexes */
#define L3_DI_INDEX_FEEL_ENGAGE                     0
#define L3_DI_INDEX_PRESS_ENGAGE                    1
#define L3_DI_INDEX_SYSTEM_ENGAGE                   2
#define L3_DI_INDEX_THROTTLE_ENABLED				8
#define L3_DI_INDEX_SAFETY_TRIP_RESET               9

/* SP Buttons data indexes */
#define L3_DI_INDEX_SP_THROTTLE_SELECT              3
#define L3_DI_INDEX_CS_WC_BUTTON_1                  8
#define L3_DI_INDEX_CS_WC_BUTTON_2                  9
#define L3_DI_INDEX_CS_WC_BUTTON_3                 10
#define L3_DI_INDEX_CS_WC_BUTTON_4                 11
#define L3_DI_INDEX_SS_BUTTON_1                    12
#define L3_DI_INDEX_SS_BUTTON_2                    13
#define L3_DI_INDEX_SS_BUTTON_3                    14
#define L3_DI_INDEX_SS_BUTTON_4                    15
#define L3_DI_INDEX_SP_FIVE_AXIS_UP                16
#define L3_DI_INDEX_SP_FIVE_AXIS_DOWN              17
#define L3_DI_INDEX_SP_FIVE_AXIS_LEFT              18
#define L3_DI_INDEX_SP_FIVE_AXIS_RIGHT             19
#define L3_DI_INDEX_SP_FIVE_AXIS_PUSH              20

/* Throttle data indexes */
#define L3_THROTTLE_INDEX_LEFT_THROTTLE_POS  		0
#define L3_THROTTLE_INDEX_RIGHT_THROTTLE_POS 		1
#define L3_THROTTLE_INDEX_LEFT_FUEL_LEVER_POS  		4
#define L3_THROTTLE_INDEX_RIGHT_FUEL_LEVER_POS 		5

/* System data indexes */
#define L3_SYSTEM_INDEX_CS_INSTALLED          		0
#define L3_SYSTEM_INDEX_WC_INSTALLED          		1
#define L3_SYSTEM_INDEX_SS_INSTALLED          		2
#define L3_SYSTEM_INDEX_SS_SELECTED           		3

/*-- STRUCTURES: ------------------------------------------------------------*/

static double * model_data = NULL;
static double * feel_data = NULL;
static double * throttle_data = NULL;
static double * discrete_data = NULL;
static double * sp_buttons_data = NULL;
static double * system_data = NULL;

/* sns_msg_ver
 * 
 * Returns supported message version for a given message id
 *
 * param: msg_id   byte containing message id
 * returns: message version stored as an unsigned byte
 */
void rpi_msg_setup()
{
    sns_msg_setup(rpi_msg_ver,
        rpi_msg_size,
        rpi_pack_message,
        rpi_process_input);
}

void rpi_setup_buffer( double * model_data_buffer, double * feel_data_buffer,
					   double * throttle_data_buffer, double * discrete_data_buffer,
					   double * sp_buttons_data_buffer, double * system_data_buffer )
{
	model_data = model_data_buffer;
	feel_data = feel_data_buffer;
	throttle_data = throttle_data_buffer;
	discrete_data = discrete_data_buffer;
	sp_buttons_data = sp_buttons_data_buffer;
	system_data = system_data_buffer;

    /* Initialize throttles */
	throttle_data[L3_THROTTLE_INDEX_LEFT_THROTTLE_POS] = 50.0  / 105.0;
	throttle_data[L3_THROTTLE_INDEX_RIGHT_THROTTLE_POS] = 50.0  / 105.0;
	throttle_data[L3_THROTTLE_INDEX_LEFT_FUEL_LEVER_POS] = 50.0  / 105.0;
	throttle_data[L3_THROTTLE_INDEX_RIGHT_FUEL_LEVER_POS] = 50.0  / 105.0;
}

unsigned char rpi_msg_ver( unsigned char msg_id )
{
    unsigned char ver = 0;

    switch( msg_id )
    {
        case MSG_ID_MRM:
            ver = MSG_VER_MRM;
            break;
        case MSG_ID_PIM:
            ver = MSG_VER_PIM;
            break;
        case MSG_ID_LMM:
            ver = MSG_VER_LMM;
            break;
        case MSG_ID_RMM:
            ver = MSG_VER_RMM;
            break;
        case MSG_ID_DM:
            ver = MSG_VER_DM;
            break;
        case MSG_ID_ASM:
            ver = MSG_VER_ASM;
            break;
        case MSG_ID_SIM:
            ver = MSG_VER_SIM;
            break;
        case MSG_ID_SFSM:
            ver = MSG_VER_SFSM;
            break;
        case MSG_ID_CFSM:
            ver = MSG_VER_CFSM;
            break;
        case MSG_ID_IM:
            ver = MSG_VER_IM;
            break;
        case MSG_ID_HUD:
            ver = MSG_VER_HUD;
            break;
        case MSG_ID_RFSM:
			ver = MSG_VER_RFSM;
			break;
        default:
            printf( "Error: invalid message id.\n" );
            ver = 255;
    }

    return ver;
}

/* sns_msg_size
 * 
 * Returns supported message size for a given message id
 *
 * param: msg_id   byte containing message id
 * returns: message size stored as an unsigned short
 */

unsigned short rpi_msg_size( unsigned char msg_id )
{
    unsigned short size = sizeof(MSG_HDR);

    switch( msg_id )
    {
        case MSG_ID_MRM:
            size += sizeof(MSG_REQ_MSG_DATA);
            break;
        case MSG_ID_PIM:
            size += sizeof(PILOT_IND_MSG_DATA);
            break;
        case MSG_ID_LMM:
            size += sizeof(LEFT_MFD_MSG_DATA);
            break;
        case MSG_ID_RMM:
            size += sizeof(RIGHT_MFD_MSG_DATA);
            break;
        case MSG_ID_DM:
            size += sizeof(DED_MSG_DATA);
            break;
        case MSG_ID_ASM:
            size += sizeof(AIR_STATE_MSG_DATA);
            break;
        case MSG_ID_SIM:
            size += sizeof(SIM_INFO_MSG_DATA);
            break;
        case MSG_ID_SFSM:
            size += sizeof(SF_SYS_MSG_DATA);
            break;
        case MSG_ID_CFSM:
            size += sizeof(CF_SYS_MSG_DATA);
            break;
        case MSG_ID_IM:
            size += sizeof(INPUT_MSG_DATA);
            break;
        case MSG_ID_HUD:
            size += sizeof(HUD_MSG_DATA);
            break;
        case MSG_ID_RFSM:
			size += sizeof(RF_SYS_MSG_DATA);
			break;
        default:
            printf( "Error: invalid message id.\n" );
            size = 0;
    }

    return size;
}

/* sns_pack_message
 * 
 * packs data according to message id for later submission to client
 *
 * param: msg_id    int containing message id
 *        data      pointer to data
 * returns: status of data packing
 */

STATUS rpi_pack_message( int msg_id, char * data )
{
    AIR_STATE_MSG_DATA *    air_state;
    CF_SYS_MSG_DATA *       centerstick_data;
    RF_SYS_MSG_DATA *       rudder_data;
	float temp = 0.0;

    switch( msg_id )
    {
        case MSG_ID_ASM:
            /* Aircraft State Message data packing */
            if( !model_data )
            {
                printf("Error: model data not accessible\n");
                return( ERROR );
            }

            air_state = (AIR_STATE_MSG_DATA *) data;

            air_state->sngl2[ASM_ALPHA] = cal_htonf( model_data[L3_MODEL_INDEX_ALPHA] );
            air_state->sngl2[ASM_PITCH] = cal_htonf( model_data[L3_MODEL_INDEX_PITCH] );
            air_state->sngl2[ASM_ROLL] = cal_htonf( model_data[L3_MODEL_INDEX_ROLL] );

            air_state->sngl[ASM_ALTITUDE] = cal_htonf( model_data[L3_MODEL_INDEX_ALTITUDE] );
            air_state->sngl[ASM_TRUE_AIRSPEED] = cal_htonf( model_data[L3_MODEL_INDEX_TRUE_AIRSPEED] );
            air_state->sngl[ASM_CALIBRATED_AIRSPEED] = cal_htonf( model_data[L3_MODEL_INDEX_CALIBRATED_AIRSPEED] );
            air_state->sngl[ASM_EQUIV_AIRSPEED] = cal_htonf( model_data[L3_MODEL_INDEX_EQUIV_AIRSPEED] );
            air_state->sngl[ASM_MACH] = cal_htonf( model_data[L3_MODEL_INDEX_MACH] );
            air_state->sngl[ASM_ALTITUDE_RATE] = cal_htonf( model_data[L3_MODEL_INDEX_ALTITUDE_RATE] );
            air_state->sngl[ASM_PLATFORM_AZIMUTH] = cal_htonf( model_data[L3_MODEL_INDEX_PLATFORM_AZIMUTH] );

            air_state->dbl[ASM_LATITUDE] = cal_htonf( model_data[L3_MODEL_INDEX_LATITUDE] );
            air_state->dbl[ASM_LONGITUDE] = cal_htonf( model_data[L3_MODEL_INDEX_LONGITUDE] );
            air_state->dbl[ASM_E_NORTH_VEL] = cal_htonf( model_data[L3_MODEL_INDEX_E_NORTH_VEL] );
            air_state->dbl[ASM_E_EAST_VEL] = cal_htonf( model_data[L3_MODEL_INDEX_E_EAST_VEL] );
            air_state->dbl[ASM_E_DOWN_VEL] = cal_htonf( -1.0 * model_data[L3_MODEL_INDEX_E_DOWN_VEL] );

            air_state->sngl2[ASM_FUEL_WEIGHT] = cal_htonf( model_data[L3_MODEL_INDEX_FUEL_WEIGHT] );;
            air_state->sngl2[ASM_NX_PILOT] = cal_htonf( model_data[L3_MODEL_INDEX_NX_PILOT] );
            air_state->sngl2[ASM_NY_PILOT] = cal_htonf( model_data[L3_MODEL_INDEX_NY_PILOT] );
            air_state->sngl2[ASM_NZ_PILOT] = cal_htonf( model_data[L3_MODEL_INDEX_NZ_PILOT] );

            air_state->sngl2[ASM_BETA] = cal_htonf( model_data[L3_MODEL_INDEX_BETA] );
            air_state->sngl2[ASM_RADAR_ALTITUDE] = cal_htonf( model_data[L3_MODEL_INDEX_RADAR_ALTITUDE] );
            air_state->sngl2[ASM_SIMULATION_TIME] = cal_htonf(ct2s(cal_gettime()));

            break;

        case MSG_ID_CFSM:
            /* Centerstick Feel System Message data packing */
/*
            centerstick_data = (CF_SYS_MSG_DATA *) data;

            rtw_get_value( &temp, "feel", "des_gradient", 0);
            centerstick_data->sngl[EFC_GRAD      ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_freq", 0);
            centerstick_data->sngl[EFC_OMEGA     ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_zeta", 0);
            centerstick_data->sngl[EFC_ZETA      ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_preload", 0);
            centerstick_data->sngl[EFC_PRELD     ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_stat_friction", 0);
            centerstick_data->sngl[EFC_FRICT     ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_breakpoint_A2", 0);
            centerstick_data->sngl[EFC_plus_B1   ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_gradient_A2", 0);
            centerstick_data->sngl[EFC_plus_M1   ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_breakpoint_A3", 0);
            centerstick_data->sngl[EFC_plus_B2   ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_gradient_A3", 0);
            centerstick_data->sngl[EFC_plus_M2   ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_breakpoint_F2", 0);
            centerstick_data->sngl[EFC_minus_B1  ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_gradient_F2", 0);
            centerstick_data->sngl[EFC_minus_M1  ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_breakpoint_F3", 0);
            centerstick_data->sngl[EFC_minus_B2  ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_gradient_F3", 0);
            centerstick_data->sngl[EFC_minus_M2  ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_min", 0);
            centerstick_data->sngl[EFC_minus_LIM ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_max", 0);
            centerstick_data->sngl[EFC_plus_LIM  ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_trim_rate", 0);
            centerstick_data->sngl[EFCP_TRMRT    ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "des_trim_max", 0);
            centerstick_data->sngl[EFCP_TRMLM    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[EFCF_TRMRT    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[EFCF_TRMLM    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[EFC_FRPLY     ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "fes_per_nz", 0);
            centerstick_data->sngl[EFCpNZ        ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "fes_per_qdot", 0);
            centerstick_data->sngl[EFCpQ_DOT     ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "fes_per_v", 0);
            centerstick_data->sngl[EFCpV         ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[EFC_CTR_PS    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[EFC_SPARE1    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[EFC_SPARE2    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[EFC_SPARE3    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[EFC_SPARE4    ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "das_gradient", 0);
            centerstick_data->sngl[AFC_GRAD      ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "das_freq", 0);
            centerstick_data->sngl[AFC_OMEGA     ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "das_zeta", 0);
            centerstick_data->sngl[AFC_ZETA      ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "das_preload", 0);
            centerstick_data->sngl[AFC_PRELD     ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "das_stat_friction", 0);
            centerstick_data->sngl[AFC_FRICT     ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "breakpoint_R2", 0);
            centerstick_data->sngl[AFC_plus_B1   ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "gradient_R2", 0);
            centerstick_data->sngl[AFC_plus_M1   ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "breakpoint_R3", 0);
            centerstick_data->sngl[AFC_plus_B2   ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "gradient_R3", 0);
            centerstick_data->sngl[AFC_plus_M2   ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "breakpoint_L2", 0);
            centerstick_data->sngl[AFC_minus_B1  ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "gradient_L2", 0);
            centerstick_data->sngl[AFC_minus_M1  ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "breakpoint_L3", 0);
            centerstick_data->sngl[AFC_minus_B2  ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "gradient_L3", 0);
            centerstick_data->sngl[AFC_minus_M2  ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "das_min", 0);
            centerstick_data->sngl[AFC_minus_LIM ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "das_max", 0);
            centerstick_data->sngl[AFC_plus_LIM  ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "das_trim_rate", 0);
            centerstick_data->sngl[AFCP_TRMRT    ] = cal_htonf( temp );

            rtw_get_value( &temp, "feel", "das_trim_max", 0);
            centerstick_data->sngl[AFCP_TRMLM    ] = cal_htonf( temp );
*/
            temp = 0.0; // TODO
            centerstick_data->sngl[AFCF_TRMRT    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[AFCF_TRMLM    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[AFC_FRPLY     ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[AFC_CTR_PS    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[AFC_SPARE1    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[AFC_SPARE2    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[AFC_SPARE3    ] = cal_htonf( temp );

            temp = 0.0; // TODO
            centerstick_data->sngl[AFC_SPARE4    ] = cal_htonf( temp );

            break;

		case MSG_ID_RFSM:
			/* Rudder Feel System Message data packing */

			rudder_data = (RF_SYS_MSG_DATA *) data;
/*
			rtw_get_value( &temp, "feel", "drp_gradient", 0);
			rudder_data->sngl[RF_GRAD      ] = cal_htonf( temp );

			rtw_get_value( &temp, "feel", "drp_freq", 0);
			rudder_data->sngl[RF_OMEGA     ] = cal_htonf( temp );

			rtw_get_value( &temp, "feel", "drp_zeta", 0);
			rudder_data->sngl[RF_ZETA      ] = cal_htonf( temp );

			rtw_get_value( &temp, "feel", "drp_preload", 0);
			rudder_data->sngl[RF_PRELD     ] = cal_htonf( temp );

			rtw_get_value( &temp, "feel", "drp_stat_friction", 0);
			rudder_data->sngl[RF_FRICT     ] = cal_htonf( temp );

			rtw_get_value( &temp, "feel", "drp_min", 0);
			rudder_data->sngl[RF_minus_LIM ] = cal_htonf( temp );

			rtw_get_value( &temp, "feel", "drp_max", 0);
			rudder_data->sngl[RF_plus_LIM  ] = cal_htonf( temp );

			rtw_get_value( &temp, "feel", "drp_trim_rate", 0);
			rudder_data->sngl[RF_TRMRT    ] = cal_htonf( temp );

			rtw_get_value( &temp, "feel", "drp_trim_max", 0);
			rudder_data->sngl[RF_TRMLM    ] = cal_htonf( temp );
*/
			break;

        default:
            printf( "Error: Invalid message id supplied for processing.\n" );
            return( ERROR );
    }

    return( OK );
}

int rpi_process_input( INPUT_MSG_DATA input[], int param_count )
{
	int i = 0;
	float tempf = 0.0;
	uint32_t tempi = 0;

	for (i = 0; i < param_count; i++)
	{
		/* Switch over grouping */

		switch (input[i].grouping)
		{
		case LEAR_DISCRETE_GROUPING:
			switch (input[i].index)
			{
				case IM_LDSG_FEEL_ENGAGE:
					tempi = cal_ntohl(input[i].value);
					discrete_data[L3_DI_INDEX_FEEL_ENGAGE] = (double) tempi;
					discrete_data[L3_DI_INDEX_THROTTLE_ENABLED] = (double) tempi;
					feel_data[L3_DI_INDEX_CS_WC_TRIM_NOSE_UP] = 1.0;
					feel_data[L3_DI_INDEX_CS_WC_TRIM_NOSE_DOWN] = 1.0;
					feel_data[L3_DI_INDEX_CS_WC_TRIM_RIGHT_WING_DOWN] = 1.0;
					feel_data[L3_DI_INDEX_CS_WC_TRIM_LEFT_WING_DOWN] = 1.0;
					feel_data[L3_DI_INDEX_SS_TRIM_NOSE_UP] = 1.0;
					feel_data[L3_DI_INDEX_SS_TRIM_NOSE_DOWN] = 1.0;
					feel_data[L3_DI_INDEX_SS_TRIM_RIGHT_WING_DOWN] = 1.0;
					feel_data[L3_DI_INDEX_SS_TRIM_LEFT_WING_DOWN] = 1.0;
					feel_data[L3_DI_INDEX_YAW_TRIM_NOSE_RIGHT] = 1.0;
					feel_data[L3_DI_INDEX_YAW_TRIM_NOSE_LEFT] = 1.0;
					break;

				case IM_LDSG_PRESS_ENGAGE:
					tempi = cal_ntohl(input[i].value);
					discrete_data[L3_DI_INDEX_PRESS_ENGAGE] = (double) tempi;
					break;

				case IM_LDSG_SYSTEM_ENGAGE:
					tempi = cal_ntohl(input[i].value);
					discrete_data[L3_DI_INDEX_SYSTEM_ENGAGE] = (double) tempi;
					break;

				case IM_LDSG_SAFETY_TRIP_RESET:
					tempi = cal_ntohl(input[i].value);
					discrete_data[L3_DI_INDEX_SAFETY_TRIP_RESET] = (double) tempi;
					break;

				case IM_LDSG_CS_WC_BUTTON_1:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_CS_WC_BUTTON_1] = (double) tempi;
					break;

				case IM_LDSG_CS_WC_BUTTON_2:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_CS_WC_BUTTON_2] = (double) tempi;
					break;

				case IM_LDSG_CS_WC_BUTTON_3:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_CS_WC_BUTTON_3] = (double) tempi;
					break;

				case IM_LDSG_CS_WC_BUTTON_4:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_CS_WC_BUTTON_4] = (double) tempi;
					break;

				case IM_LDSG_SS_BUTTON_1:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_SS_BUTTON_1] = (double) tempi;
					break;

				case IM_LDSG_SS_BUTTON_2:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_SS_BUTTON_2] = (double) tempi;
					break;

				case IM_LDSG_SS_BUTTON_3:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_SS_BUTTON_3] = (double) tempi;
					break;

				case IM_LDSG_SS_BUTTON_4:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_SS_BUTTON_4] = (double) tempi;
					break;

				case IM_LDSG_SP_FIVE_AXIS_UP:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_SP_FIVE_AXIS_UP] = (double) tempi;
					break;

				case IM_LDSG_SP_FIVE_AXIS_DOWN:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_SP_FIVE_AXIS_DOWN] = (double) tempi;
					break;

				case IM_LDSG_SP_FIVE_AXIS_LEFT:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_SP_FIVE_AXIS_LEFT] = (double) tempi;
					break;

				case IM_LDSG_SP_FIVE_AXIS_RIGHT:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_SP_FIVE_AXIS_RIGHT] = (double) tempi;
					break;

				case IM_LDSG_SP_FIVE_AXIS_PUSH:
					tempi = cal_ntohl(input[i].value);
					sp_buttons_data[L3_DI_INDEX_SP_FIVE_AXIS_PUSH] = (double) tempi;
					break;

				case IM_LDSG_CS_WC_TRIM_NOSE_UP:
					tempi = cal_ntohl(input[i].value) ^ 1;
					feel_data[L3_DI_INDEX_CS_WC_TRIM_NOSE_UP] = (double) tempi;
					break;

				case IM_LDSG_CS_WC_TRIM_NOSE_DOWN:
					tempi = cal_ntohl(input[i].value) ^ 1;
					feel_data[L3_DI_INDEX_CS_WC_TRIM_NOSE_DOWN] = (double) tempi;
					break;

				case IM_LDSG_CS_WC_TRIM_RIGHT_WING_DOWN:
					tempi = cal_ntohl(input[i].value) ^ 1;
					feel_data[L3_DI_INDEX_CS_WC_TRIM_RIGHT_WING_DOWN] = (double) tempi;
					break;

				case IM_LDSG_CS_WC_TRIM_LEFT_WING_DOWN:
					tempi = cal_ntohl(input[i].value) ^ 1;
					feel_data[L3_DI_INDEX_CS_WC_TRIM_LEFT_WING_DOWN] = (double) tempi;
					break;

				case IM_LDSG_SS_TRIM_NOSE_UP:
					tempi = cal_ntohl(input[i].value) ^ 1;
					feel_data[L3_DI_INDEX_SS_TRIM_NOSE_UP] = (double) tempi;
					break;

				case IM_LDSG_SS_TRIM_NOSE_DOWN:
					tempi = cal_ntohl(input[i].value) ^ 1;
					feel_data[L3_DI_INDEX_SS_TRIM_NOSE_DOWN] = (double) tempi;
					break;

				case IM_LDSG_SS_TRIM_RIGHT_WING_DOWN:
					tempi = cal_ntohl(input[i].value) ^ 1;
					feel_data[L3_DI_INDEX_SS_TRIM_RIGHT_WING_DOWN] = (double) tempi;
					break;

				case IM_LDSG_SS_TRIM_LEFT_WING_DOWN:
					tempi = cal_ntohl(input[i].value) ^ 1;
					feel_data[L3_DI_INDEX_SS_TRIM_LEFT_WING_DOWN] = (double) tempi;
					break;

				case IM_LDSG_YAW_TRIM_NOSE_RIGHT:
					tempi = cal_ntohl(input[i].value) ^ 1;
					feel_data[L3_DI_INDEX_YAW_TRIM_NOSE_RIGHT] = (double) tempi;
					break;

				case IM_LDSG_YAW_TRIM_NOSE_LEFT:
					tempi = cal_ntohl(input[i].value) ^ 1;
					feel_data[L3_DI_INDEX_YAW_TRIM_NOSE_LEFT] = (double) tempi;
					break;

				case IM_LDSG_CS_INSTALLED:
					tempi = cal_ntohl(input[i].value) ^ 1;
					system_data[L3_SYSTEM_INDEX_CS_INSTALLED] = (double) tempi;
					break;

				case IM_LDSG_WC_INSTALLED:
					tempi = cal_ntohl(input[i].value) ^ 1;
					system_data[L3_SYSTEM_INDEX_WC_INSTALLED] = (double) tempi;
					break;

				case IM_LDSG_SS_INSTALLED:
					tempi = cal_ntohl(input[i].value) ^ 1;
					system_data[L3_SYSTEM_INDEX_SS_INSTALLED] = (double) tempi;
					break;

				case IM_LDSG_SS_SELECTED:
					tempi = cal_ntohl(input[i].value) ^ 1;
					system_data[L3_SYSTEM_INDEX_SS_SELECTED] = (double) tempi;
					break;

				default:
					break;
			}

			break;

		case LEAR_PILOT_CTRL_GROUPING:
			switch (input[i].index)
			{
				case IM_LPCG_CS_PITCH_FORCE:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_CS_PITCH_FORCE] = tempf / 100.0;
					break;

				case IM_LPCG_CS_PITCH_POSITION:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_CS_PITCH_POSITION] = tempf / -10.0;
					break;

				case IM_LPCG_CS_ROLL_FORCE:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_CS_ROLL_FORCE] = tempf / 100.0;
					break;

				case IM_LPCG_CS_ROLL_POSITION:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );;
					feel_data[L3_FEEL_INDEX_CS_ROLL_POSITION] = tempf / -5.0;
					break;

				case IM_LPCG_WC_PITCH_FORCE:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_WC_PITCH_FORCE] = tempf / 200.0;
					break;

				case IM_LPCG_WC_PITCH_POSITION:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_WC_PITCH_POSITION] = tempf / -10.0;
					break;

				case IM_LPCG_WC_ROLL_FORCE:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_WC_ROLL_FORCE] = tempf / 100.0;
					break;

				case IM_LPCG_WC_ROLL_POSITION:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_WC_ROLL_POSITION] = tempf / -200.0;
					break;

				case IM_LPCG_SS_PITCH_FORCE:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_SS_PITCH_FORCE] = tempf / 50.0;
					break;

				case IM_LPCG_SS_PITCH_POSITION:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_SS_PITCH_POSITION] = tempf / -25.0;
					break;

				case IM_LPCG_SS_ROLL_FORCE:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_SS_ROLL_FORCE] = tempf / 25.0;
					break;

				case IM_LPCG_SS_ROLL_POSITION:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_SS_ROLL_POSITION] = tempf / -25.0;
					break;

				case IM_LPCG_RP_FORCE:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_RP_FORCE] = tempf / 200.0;
					break;

				case IM_LPCG_RP_POSITION:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					feel_data[L3_FEEL_INDEX_RP_POSITION] = tempf / -2.5;
					break;

				case IM_LPCG_LEFT_THROTTLE_POSITION:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					throttle_data[L3_THROTTLE_INDEX_LEFT_THROTTLE_POS] = tempf / 105.0;
					throttle_data[L3_THROTTLE_INDEX_LEFT_FUEL_LEVER_POS] = tempf  / 105.0;
					break;

				case IM_LPCG_RIGHT_THROTTLE_POSITON:
					tempi = cal_ntohl(input[i].value);
					memcpy( &tempf, &tempi, sizeof(float) );
					throttle_data[L3_THROTTLE_INDEX_RIGHT_THROTTLE_POS] = tempf  / 105.0;
					throttle_data[L3_THROTTLE_INDEX_RIGHT_FUEL_LEVER_POS] = tempf  / 105.0;
					break;

				default:
					break;
			}

			break;

		default:
			printf("Unknown input: %i %i\n", input[i].grouping, input[i].index );
			break;
		}
	}

	free( input );

	return (0);
}
