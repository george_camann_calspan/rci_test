################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../client.c \
../rpi_msg_handler.c \
../sns_msg_handler.c 

OBJS += \
./client.o \
./rpi_msg_handler.o \
./sns_msg_handler.o 

C_DEPS += \
./client.d \
./rpi_msg_handler.d \
./sns_msg_handler.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


