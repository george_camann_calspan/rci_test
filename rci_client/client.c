/*
 * client.c

 *
 *  Created on: Jul 25, 2016
 *      Author: paul
 */

#include "../src/rpi_msg_handler.h"
#include "cal_types.h"

#include "cal_constants.h"
#include "cal_platform.h"
#include "cal_time.h"
#include "cal_crc32.h"
#include "cal_swap.h"

#include <sys/socket.h>
#include <stdio.h>

#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#define LOCAL_IP_LEN (32)

STATUS send_mrm( int id, int ver, int rate, int duration, int port, int sFd )
{
    /* Build client socket address */

    struct sockaddr_in  server_addr;    /* clients's socket address */
    unsigned int        sock_addr_size;/* size of socket address structure */
    uint32_t server_ip_addr;
    uint32_t client_ip_addr;
    int index = 0;

	cal_addr_hex_get( "127.0.0.1", &server_ip_addr );
	char local_ip_str[LOCAL_IP_LEN];

	sock_addr_size = sizeof (struct sockaddr_in);
    bzero ((char *) &server_addr, sock_addr_size);
#ifdef __vxworks
    server_addr.sin_len = (u_char) sock_addr_size;
#endif
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = cal_htons(port); //SNS_PORT_NUM
    server_addr.sin_addr.s_addr = cal_htonl(server_ip_addr);

    /* Setup message request message */
    MSG_REQ_MSG send_msg;
    CAL_TIME_T timestamp = cal_getclock();

    memset( &send_msg, 0, sizeof( MSG_REQ_MSG ) );

    /* get local address */
    while(TRUE)
    {
    	cal_local_addr_get( local_ip_str, LOCAL_IP_LEN, index);
    	cal_addr_hex_get( local_ip_str, &client_ip_addr );
    	if( 0x7F000001 != client_ip_addr) break;			//fails if client_ip is 127.0.0.1
        index ++;
    }
    printf( "Using local IP of %s\n", local_ip_str );


    /* Build message request message header */

    send_msg.header.msg_size = rpi_msg_size( MSG_ID_MRM );
    send_msg.header.msg_id = MSG_ID_MRM;
    send_msg.header.msg_ver = 0;
    send_msg.header.msg_time_msec = timestamp.sec * 1000 + timestamp.nsec / 1000000;

    send_msg.data.msg_duration = (unsigned char) duration;
    send_msg.data.msg_rate = (unsigned char) rate;
    send_msg.data.ip_addr = client_ip_addr;
    send_msg.data.port = RPI_PORT_NUM;
    send_msg.data.msg_id = (unsigned char) id;
    send_msg.data.msg_ver = (unsigned char) ver;

    /* Swap necessary values for big endian */

    send_msg.header.msg_size = cal_htons( send_msg.header.msg_size );
    send_msg.data.ip_addr = cal_htonl( send_msg.data.ip_addr );
    send_msg.data.port = cal_htons( send_msg.data.port );

    /* Checksum */

    send_msg.header.checksum =
            crc32_full( (unsigned char *) &send_msg.header.msg_size,
            			cal_ntohs(send_msg.header.msg_size) - 4 );
    send_msg.header.checksum = cal_htonl( send_msg.header.checksum );

    /* Send response to client */
    if( sFd )
    {
		if ( sendto (sFd, (caddr_t) &send_msg, cal_ntohs(send_msg.header.msg_size), 0,
						(struct sockaddr *) &server_addr, sock_addr_size) == ERROR )
		{
			perror( "sendto" );
			printf( "Trying to send to IP address %i.%i.%i.%i and port %i\n",
					*(((char *) &server_addr.sin_addr.s_addr)+3),
					*(((char *) &server_addr.sin_addr.s_addr)+2),
					*(((char *) &server_addr.sin_addr.s_addr)+1),
					*(((char *) &server_addr.sin_addr.s_addr)+0),
					server_addr.sin_port );
			printf( "Could not reach IP address. Shutting down client.\n" );
			close( sFd );
			return( ERROR );
		}
    }
    else
    {
    	printf("File descriptor not initialized\n");
    }

    return( OK );
}
int main()
{
	int sFd;
    if((sFd = socket(AF_INET, SOCK_DGRAM, 0)) == ERROR)
    {
    	perror("socket");
    	return ERROR;
    }

    send_mrm( 0, 0, 0, 1, 1405, sFd);

    send_mrm( 0, 0, 0, 0, 1405, sFd);
	return 0;
}



