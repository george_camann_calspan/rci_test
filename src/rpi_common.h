#ifndef RPI_COMMON_H
#define RPI_COMMON_H

/*-- CPP COMPLIANT: ---------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*-- DEFINES: ---------------------------------------------------------------*/

#define SNS_PORT_NUM       1400 /* server recv port number */ 
#define VISTA_RCI_PORT_NUM 1401 /* VISTA RCI recv port number */
#define HB_SCRIPT_PORT_NUM 1402 /* HB Scripting recv port number */  
#define FEEL_SYS_PORT_NUM  1403 /* Feel System Tester recv port number */
#define HUD_PORT_NUM       1404 /* HUD Tester recv port number */
#define RPI_PORT_NUM       1405 /* RPI Tester recv port number */

/* Message IDs */

#define MSG_ID_MRM      0       /* Message Request Message */
#define MSG_ID_PIM      1       /* Pilot Indication Message */
#define MSG_ID_LMM      2       /* Left MFD Mesage */
#define MSG_ID_RMM      3       /* Right MFD Message */
#define MSG_ID_DM       4       /* DED Message */
#define MSG_ID_ASM      5       /* Aircraft State Message */
#define MSG_ID_SIM      6       /* Simulation Information Message */
#define MSG_ID_SFSM     7       /* Sidestick Feel System Message */
#define MSG_ID_CFSM     8       /* Centerstick Feel System Message */
#define MSG_ID_IM       9       /* Input Message */
#define MSG_ID_HUD	   10		/* HUD Message */
#define MSG_ID_RFSM    11		/* Rudder Feel System Message */

#define MSG_ID_MAX     11       /* Message ID Maximum */

/* Current versions */

#define MSG_VER_MRM     0       /* Current Message Request Message Version */
#define MSG_VER_PIM     0       /* Pilot Indication Message Version */
#define MSG_VER_LMM     0       /* Left MFD Mesage Version */
#define MSG_VER_RMM     0       /* Right MFD Message Version */
#define MSG_VER_DM      0       /* DED Message Version */
#define MSG_VER_ASM     0       /* Aircraft State Message Version */
#define MSG_VER_SIM     0       /* Simulation Information Message Version */
#define MSG_VER_SFSM    0       /* Sidestick Feel System Message Version */
#define MSG_VER_CFSM    0       /* Centerstick Feel System Message Version */
#define MSG_VER_IM      0       /* Input Message */
#define MSG_VER_RFSM    0		/* Rudder Feel System Message */
#define MSG_VER_HUD		0		/* HUD Message */	

/*-- TYPEDEFS: --------------------------------------------------------------*/

/* ICD Structures */

#pragma pack(1)

typedef struct RpiMessageHeader {    
    unsigned int   checksum;      /* 00 Checksum of message */
    unsigned short msg_size;      /* 04 Size of msg in bytes, includes header.*/
    unsigned char  msg_id;        /* 06 ID of this message type */
    unsigned char  msg_ver;       /* 07 Version of this message type id */
    unsigned int   msg_time_msec; /* 08 Message time in milliseconds */
    unsigned int   reserved;      /* 0C Reserved for future use */
} MSG_HDR;                        /* 10 - Total Size */

typedef struct RpiMessageRequestMessageData {
    unsigned int   ip_addr;     /* 00 IP address of client to send message to */
    unsigned short port;        /* 04 UDP Port number of client to send msg to*/
    unsigned char  msg_id;      /* 06 ID of requested message */
    unsigned char  msg_ver;     /* 07 Version of requested message */
    unsigned char  msg_rate;    /* 08 Rate to send message at */
    unsigned char  msg_duration;/* 09 Number of seconds to send message for */
} MSG_REQ_MSG_DATA;             /* 0A - Total Size */

typedef struct PilotIndicationMessageData {
#define VHV_BOOL_TOTAL 96
    unsigned char   vhv_bool[12];   /* 00 Boolean values (VHV01) */
#define VHV_BCD_TOTAL 30
    unsigned char   vhv_bcd[30];    /* 0C Binary coded decimal values (VHV01) */
#define VHV_I16_TOTAL 16        
    signed short    vhv_i16[16];    /* 2A 16-bit signed integer values (VHV01)*/
#define VHV_BCD2_TOTAL 2        
    unsigned char   vhv_bcd2[2];    /* 4A Binary coded decimal values (VHV01) */
    float           dthc;           /* 4C DHTC VSS Analog Output */
    float           cpc_sngl[5];    /* 50 Single precision floats (CPC) */
#define CPC_SNGL_TOTAL 5
    unsigned char   cpc_rec_num;    /* 60 CPC Record Number */
    unsigned char   cpc_bool;       /* 61 Boolean values (CPC) */
    unsigned char   vss_do_bool[7]; /* 62 Boolean values (VSS Discrete Output)*/
                                        /* 3 bits of last byte reserved */
    unsigned char   reserved;       /* 69 Reserved */
    unsigned char   vss_di_bool[7]; /* 6A Boolean values (VSS Discrete Input) */
    unsigned char   reserved2;      /* 71 Reserved */
    unsigned char   gsc_d_bool[2];  /* 72 Boolean values (GSC Discrete) */
                                        /* 7 bits of last byte reserved */
    unsigned char   vss_i_bool;     /* 74 Boolean values (VSS Input Loop Back)*/ 
                                        /* 5 bits of this byte reserved */
    unsigned char   reserved3[3];   /* 75 Reserved */
} PILOT_IND_MSG_DATA;               /* 78 - Total Size */

typedef struct LeftFrontMfdMessage {
    unsigned char   text[20][30];   /* 000 MFD Text (20 lines @ 30 characters)*/
        #define MFD_LINE_LENGTH 30
        #define MFD_LINE_COUNT  20
} LEFT_MFD_MSG_DATA;                /* 258 - Total Size */

typedef struct RightFrontMfdMessage {
    unsigned char   text[20][30];   /* 000 MFD Text (20 lines @ 30 characters)*/
} RIGHT_MFD_MSG_DATA;               /* 258 - Total Size */
                                    
typedef struct DedMessage {
    unsigned char   text[5][24];    /* 00 DED Text (5 lines of 25 characters)*/
        #define DECIS_LINE_LENGTH 24
        #define DECIS_LINE_COUNT   5
    unsigned char   reserved[3];    /* 7D Reserved */
} DED_MSG_DATA;                     /* 80 - Total Size */

typedef struct AircraftStateMessage {
    float   sngl[7];                /* 00 Single precision floats */
        #define ASM_ALTITUDE            0
        #define ASM_TRUE_AIRSPEED       1
        #define ASM_CALIBRATED_AIRSPEED 2
        #define ASM_EQUIV_AIRSPEED      3
        #define ASM_MACH                4
        #define ASM_ALTITUDE_RATE       5
        #define ASM_PLATFORM_AZIMUTH    6
        #define ASM_SNGL_MAX            7
    double  dbl[5];                 /* 1C Double precision floats */
        #define ASM_LATITUDE            0
        #define ASM_LONGITUDE           1
        #define ASM_E_NORTH_VEL         2
        #define ASM_E_EAST_VEL          3
        #define ASM_E_DOWN_VEL          4
        #define ASM_DBL_MAX             5
    float   sngl2[11];              /* 44 Single precision floats */
        #define ASM_FUEL_WEIGHT         0
        #define ASM_NX_PILOT            1
        #define ASM_NY_PILOT            2
        #define ASM_NZ_PILOT            3
        #define ASM_ALPHA               4
        #define ASM_BETA                5
        #define ASM_PITCH               6
        #define ASM_ROLL                7
        #define ASM_RADAR_ALTITUDE      8
        #define ASM_SIMULATION_TIME     9
        #define ASM_DELTA_TIME          10
        #define ASM_SNGL2_MAX           11
} AIR_STATE_MSG_DATA;               /* 70 - Total Size */

typedef struct SimulationInformationMessage {
    float           delta_time;        /* 00 Host Sim Delta Time */
    unsigned char   bytes[2];       /* 04 Bytes */
        #define HOST_SIM_STATE          0
        #define INITIAL_CONDITION_NUM   1
        #define SIM_BYTES_MAX           2
        /* Host Sim State */
        #define HSS_TRIMMED             0
        #define HSS_RUNNING             1
        #define HSS_STOPPED             2
        #define HSS_TRIMMING            3 /* Not currently used */
    unsigned char   bools;          /* 06 Boolean values */
        #define TANK_ON_300G            1 << 0
        #define TANK_ON_740G            1 << 1
        #define WING_TIP_MISSLES_ON     1 << 2
        #define LANDING_GEAR_DOWN       1 << 3
    unsigned char   reserved;       /* 07 Reserved */ 
    float           sngl[12];       /* 08 Single precision floats */
        #define TRIM_SPEEDBRAKE         0
        #define TRIM_TRUE_AIRSPEED      1
        #define TRIM_CAL_AIRSPEED       2
        #define TRIM_EQUIV_AIRSPEED     3
        #define TRIM_ALTITUDE           4
        #define TRIM_MACH               5
        #define TRIM_ALPHA              6
        #define TRIM_LATITUDE           7
        #define TRIM_LONGITUDE          8
        #define TRIM_TRUE_HEADING       9
        #define TRIM_PLA_COMMAND        10
        #define TRIM_FUEL_WEIGHT        11
        #define SIM_SNGL_MAX            12
} SIM_INFO_MSG_DATA;                /* 38 - Total Size */

typedef struct SidestickFeelSystemMessage {
    float   sngl[54];               /* 00 Single precision floats */
} SF_SYS_MSG_DATA;                  /* D8 - Total Size */

typedef struct CenterstickFeelSystemMessage {
    float   sngl[53];               /* 00 Single precision floats */
        #define EFC_GRAD                0
        #define EFC_OMEGA               1
        #define EFC_ZETA                2
        #define EFC_PRELD               3
        #define EFC_FRICT               4
        #define EFC_plus_B1             5
        #define EFC_plus_M1             6
        #define EFC_plus_B2             7
        #define EFC_plus_M2             8
        #define EFC_minus_B1            9
        #define EFC_minus_M1           10
        #define EFC_minus_B2           11
        #define EFC_minus_M2           12
        #define EFC_minus_LIM          13
        #define EFC_plus_LIM           14
        #define EFCP_TRMRT             15
        #define EFCP_TRMLM             16
        #define EFCF_TRMRT             17
        #define EFCF_TRMLM             18
        #define EFC_FRPLY              19
        #define EFCpNZ                 20
        #define EFCpQ_DOT              21
        #define EFCpV                  22
        #define EFC_CTR_PS             23
        #define EFC_SPARE1             24
        #define EFC_SPARE2             25
        #define EFC_SPARE3             26
        #define EFC_SPARE4             27
        #define AFC_GRAD               28
        #define AFC_OMEGA              29
        #define AFC_ZETA               30
        #define AFC_PRELD              31
        #define AFC_FRICT              32
        #define AFC_plus_B1            33
        #define AFC_plus_M1            34
        #define AFC_plus_B2            35
        #define AFC_plus_M2            36
        #define AFC_minus_B1           37
        #define AFC_minus_M1           38
        #define AFC_minus_B2           39
        #define AFC_minus_M2           40
        #define AFC_minus_LIM          41
        #define AFC_plus_LIM           42
        #define AFCP_TRMRT             43
        #define AFCP_TRMLM             44
        #define AFCF_TRMRT             45
        #define AFCF_TRMLM             46
        #define AFC_FRPLY              47
        #define AFC_CTR_PS             48
        #define AFC_SPARE1             49
        #define AFC_SPARE2             50
        #define AFC_SPARE3             51
        #define AFC_SPARE4             52
} CF_SYS_MSG_DATA;                  /* D4 - Total Size */

typedef struct RudderFeelSystemMessage {
    float   sngl[13];               /* 00 Single precision floats */
        #define RF_GRAD                 0
        #define RF_OMEGA                1
        #define RF_ZETA                 2
        #define RF_PRELD                3
        #define RF_FRICT                4
        #define RF_minus_LIM            5
        #define RF_plus_LIM             6
        #define RF_TRMRT                7
        #define RF_TRMLM                8
        #define RF_SPARE1               9
        #define RF_SPARE2              10
        #define RF_SPARE3              11
        #define RF_SPARE4              12
} RF_SYS_MSG_DATA;                  /* 34 - Total Size */

typedef struct InputMessage {
    unsigned char   grouping;       /* 00 Grouping */
    unsigned char   index;          /* 01 Index */
    unsigned char   reserved[2];    /* 02 Reserved */
    unsigned long   value;          /* 04 Value */
} INPUT_MSG_DATA;                   /* 08 - Total Size */

typedef struct HudMessage {
    unsigned char   line_en;        /* 00 Line Enable */
										/* Bit 0 target line visibility */
										/* Bit 1 upper boundary line visibility*/
										/* Bit 2 lower boundary line visibility*/
    unsigned char   reserved[3];    /* 01 Reserved */
    float		    tgt_len;        /* 04 Length (width) of target/boundary lines */
	float		    gc_len;         /* 08 Height and width of gun cross */
	float		    ac_ref_len;     /* 0A Elevation of gun cross above/below center */
	float		    tgt_el;         /* 10 Elevation of target line above/below gun cross */
	float		    bl_offset;      /* 14 Elevation of boundary line above/below target line */
} HUD_MSG_DATA;                   /* 18 - Total Size */
                                    
typedef struct Message {
    MSG_HDR             header;     /* 000 Message header */
    union {                         /* 010 Message data types */
        MSG_REQ_MSG_DATA    msg_req_msg;        /* Message request message */
        PILOT_IND_MSG_DATA  pilot_ind_msg;      /* Pilot indication message */
        LEFT_MFD_MSG_DATA   left_mfd_msg;       /* Left MFD message */
        RIGHT_MFD_MSG_DATA  right_mfd_msg;      /* Right MFD message */
        DED_MSG_DATA        ded_msg;            /* DED message */
        AIR_STATE_MSG_DATA  air_state_msg;      /* Aircraft State  message */
        SIM_INFO_MSG_DATA   sim_info_msg;       /* Simulation Info message */
        SF_SYS_MSG_DATA     sf_sys_msg;         /* Sidestick Feel System msg */
        CF_SYS_MSG_DATA     cf_sys_msg;         /* Centerstick Feel System msg*/
        INPUT_MSG_DATA      input_msg[32];      /* Input message */
        HUD_MSG_DATA        hud_msg;            /* HUD message */
        RF_SYS_MSG_DATA		rf_sys_msg;			/* Rudder Feel System msg */
        char                bytes[0x258];       /* Byte-addressable data */
    } data;
} SNS_MSG;                              /* 268 - Total Size */    

typedef struct MessageRequestMessage {
    MSG_HDR             header;     /* 00 Message Header */
    MSG_REQ_MSG_DATA    data;       /* 10 Message Request Message Data */
} MSG_REQ_MSG;                      /* 1A - Total Size */

#pragma pack()
   
/* Data Indexes */
#ifdef THESE_ARE_IN_VMUX_REPOSITORY_UNIT
/* "enumeration" of VR_B_INDEX_TYPE */
#define VR_B_INDEX_FIRST                           0
/*-- VHV : VSS -> CPC --*/
#define VR_B_VHV01_FRESH_DATA                      0
#define VR_B_VHV01_NEW_RECORD_NUMBER               1
#define VR_B_VHV01_SPARE_PRELOAD                   2
#define VR_B_VHV01_RUD_PRELOAD                     3
#define VR_B_VHV01_AIL_PRELOAD                     4
#define VR_B_VHV01_ELEV_PRELOAD                    5
#define VR_B_VHV01_SPARE_FRICTION                  6
#define VR_B_VHV01_RUD_FRICTION                    7
#define VR_B_VHV01_AIL_FRICTION                    8
#define VR_B_VHV01_ELEV_FRICTION                   9
#define VR_B_VHV01_SPARE_FREEPLY                   10
#define VR_B_VHV01_RUD_FREEPLY                     11
#define VR_B_VHV01_AIL_FREEPLY                     12
#define VR_B_VHV01_ELEV_FREEPLY                    13
#define VR_B_VHV01_SPARE_NON_LIN                   14
#define VR_B_VHV01_RUD_NON_LIN                     15
#define VR_B_VHV01_AIL_NON_LIN                     16
#define VR_B_VHV01_ELEV_NON_LIN                    17
#define VR_B_VHV01_SPARE_FORCE                     18
#define VR_B_VHV01_RUD_FORCE                       19
#define VR_B_VHV01_AIL_FORCE                       20
#define VR_B_VHV01_ELEV_FORCE                      21
#define VR_B_VHV01_SPARE_POSITION                  22
#define VR_B_VHV01_RUD_POSITION                    23
#define VR_B_VHV01_AIL_POSITION                    24
#define VR_B_VHV01_ELEV_POSITION                   25
#define VR_B_VHV01_SPARE_LEAD_LAG                  26
#define VR_B_VHV01_RUD_LEAD_LAG                    27
#define VR_B_VHV01_AIL_LEAD_LAG                    28
#define VR_B_VHV01_ELEV_LEAD_LAG                   29
#define VR_B_VHV01_SPARE_DELAY                     30
#define VR_B_VHV01_RUD_DELAY                       31
#define VR_B_VHV01_AIL_DELAY                       32
#define VR_B_VHV01_ELEV_DELAY                      33
#define VR_B_VHV01_SPARE_SPECIAL                   34
#define VR_B_VHV01_RUD_SPECIAL                     35
#define VR_B_VHV01_AIL_SPECIAL                     36
#define VR_B_VHV01_ELEV_SPECIAL                    37
#define VR_B_VHV01_SPARE_SURF_TRIM                 38
#define VR_B_VHV01_RUD_SURF_TRIM                   39
#define VR_B_VHV01_AIL_SURF_TRIM                   40
#define VR_B_VHV01_ELEV_SURF_TRIM                  41
#define VR_B_VHV01_SPARE_POS_TRIM                  42
#define VR_B_VHV01_RUD_POS_TRIM                    43
#define VR_B_VHV01_AIL_POS_TRIM                    44
#define VR_B_VHV01_ELEV_POS_TRIM                   45
#define VR_B_VHV01_SPARE_FORCE_TRIM                46
#define VR_B_VHV01_RUD_FORCE_TRIM                  47
#define VR_B_VHV01_AIL_FORCE_TRIM                  48
#define VR_B_VHV01_ELEV_FORCE_TRIM                 49
#define VR_B_VHV01_RECORD_NUMBER_LOAD              50
#define VR_B_VHV01_TITAN_BOARD_FAIL_LITE           51
#define VR_B_VHV01_TITAN_FAIL_LITE                 52
#define VR_B_VHV01_TITAN_READY_LITE                53
#define VR_B_VHV01_FORE_ELEVATOR_FORCE_DECIMAL_TENS_ONES 54
#define VR_B_VHV01_FORE_ELEVATOR_FORCE_DECIMAL_ONE_TENTHS 55
#define VR_B_VHV01_FORE_ELEVATOR_FORCE_DECIMAL_TENTHS 56
#define VR_B_VHV01_FORE_AILERON_FORCE_DECIMAL_TENS_ONES 57
#define VR_B_VHV01_FORE_AILERON_FORCE_DECIMAL_ONE_TENTHS 58
#define VR_B_VHV01_FORE_AILERON_FORCE_DECIMAL_TENTHS 59
#define VR_B_VHV01_FORE_RUDDER_FORCE_DECIMAL_TENS_ONES 60
#define VR_B_VHV01_FORE_RUDDER_FORCE_DECIMAL_ONE_TENTHS 61
#define VR_B_VHV01_FORE_RUDDER_FORCE_DECIMAL_TENTHS 62
#define VR_B_VHV01_AFT_ELEVATOR_FORCE_DECIMAL_TENS_ONES 63
#define VR_B_VHV01_AFT_ELEVATOR_FORCE_DECIMAL_ONE_TENTHS 64
#define VR_B_VHV01_AFT_ELEVATOR_FORCE_DECIMAL_TENTHS 65
#define VR_B_VHV01_AFT_AILERON_FORCE_DECIMAL_TENS_ONES 66
#define VR_B_VHV01_AFT_AILERON_FORCE_DECIMAL_ONE_TENTHS 67
#define VR_B_VHV01_AFT_AILERON_FORCE_DECIMAL_TENTHS 68
#define VR_B_VHV01_AFT_RUDDER_FORCE_DECIMAL_TENS_ONES 69
#define VR_B_VHV01_AFT_RUDDER_FORCE_DECIMAL_ONE_TENTHS 70
#define VR_B_VHV01_AFT_RUDDER_FORCE_DECIMAL_TENTHS 71
#define VR_B_VHV01_FORE_ELEVATOR_POSITION_DECIMAL_ONE_TENTHS 72
#define VR_B_VHV01_FORE_ELEVATOR_POSITION_DECIMAL_TENTHS 73
#define VR_B_VHV01_FORE_ELEVATOR_POSITION_NEGATIVE_SIGN 74
#define VR_B_VHV01_FORE_ELEVATOR_POSITION_POSITIVE_SIGN 75
#define VR_B_VHV01_FORE_AILERON_POSITION_DECIMAL_ONE_TENTHS 76
#define VR_B_VHV01_FORE_AILERON_POSITION_DECIMAL_TENTHS 77
#define VR_B_VHV01_FORE_AILERON_POSITION_NEGATIVE_SIGN 78
#define VR_B_VHV01_FORE_AILERON_POSITION_POSITIVE_SIGN 79
#define VR_B_VHV01_FORE_RUDDER_POSITION_DECIMAL_ONE_TENTHS 80
#define VR_B_VHV01_FORE_RUDDER_POSITION_DECIMAL_TENTHS 81
#define VR_B_VHV01_FORE_RUDDER_POSITION_NEGATIVE_SIGN 82
#define VR_B_VHV01_FORE_RUDDER_POSITION_POSITIVE_SIGN 83
#define VR_B_VHV01_AFT_ELEVATOR_POSITION_DECIMAL_ONE_TENTHS 84
#define VR_B_VHV01_AFT_ELEVATOR_POSITION_DECIMAL_TENTHS 85
#define VR_B_VHV01_AFT_ELEVATOR_POSITION_NEGATIVE_SIGN 86
#define VR_B_VHV01_AFT_ELEVATOR_POSITION_POSITIVE_SIGN 87
#define VR_B_VHV01_AFT_AILERON_POSITION_DECIMAL_ONE_TENTHS 88
#define VR_B_VHV01_AFT_AILERON_POSITION_DECIMAL_TENTHS 89
#define VR_B_VHV01_AFT_AILERON_POSITION_NEGATIVE_SIGN 90
#define VR_B_VHV01_AFT_AILERON_POSITION_POSITIVE_SIGN 91
#define VR_B_VHV01_AFT_RUDDER_POSITION_DECIMAL_ONE_TENTHS 92
#define VR_B_VHV01_AFT_RUDDER_POSITION_DECIMAL_TENTHS 93
#define VR_B_VHV01_AFT_RUDDER_POSITION_NEGATIVE_SIGN 94
#define VR_B_VHV01_AFT_RUDDER_POSITION_POSITIVE_SIGN 95
/*-- VVH : CPC -> VSS --*/
#define VR_B_VVH01_FRESH_DATA                      96
#define VR_B_VVH01_SPARE_PRELOAD                   97
#define VR_B_VVH01_RUD_PRELOAD                     98
#define VR_B_VVH01_AIL_PRELOAD                     99
#define VR_B_VVH01_ELEV_PRELOAD                    100
#define VR_B_VVH01_SPARE_FRICTION                  101
#define VR_B_VVH01_RUD_FRICTION                    102
#define VR_B_VVH01_AIL_FRICTION                    103
#define VR_B_VVH01_ELEV_FRICTION                   104
#define VR_B_VVH01_SPARE_FREEPLY                   105
#define VR_B_VVH01_RUD_FREEPLY                     106
#define VR_B_VVH01_AIL_FREEPLY                     107
#define VR_B_VVH01_ELEV_FREEPLY                    108
#define VR_B_VVH01_SPARE_NON_LIN                   109
#define VR_B_VVH01_RUD_NON_LIN                     110
#define VR_B_VVH01_AIL_NON_LIN                     111
#define VR_B_VVH01_ELEV_NON_LIN                    112
#define VR_B_VVH01_SPARE_FORCE                     113
#define VR_B_VVH01_RUD_FORCE                       114
#define VR_B_VVH01_AIL_FORCE                       115
#define VR_B_VVH01_ELEV_FORCE                      116
#define VR_B_VVH01_SPARE_POSITION                  117
#define VR_B_VVH01_RUD_POSITION                    118
#define VR_B_VVH01_AIL_POSITION                    119
#define VR_B_VVH01_ELEV_POSITION                   120
#define VR_B_VVH01_SPARE_LEAD_LAG                  121
#define VR_B_VVH01_RUD_LEAD_LAG                    122
#define VR_B_VVH01_AIL_LEAD_LAG                    123
#define VR_B_VVH01_ELEV_LEAD_LAG                   124
#define VR_B_VVH01_SPARE_DELAY                     125
#define VR_B_VVH01_RUD_DELAY                       126
#define VR_B_VVH01_AIL_DELAY                       127
#define VR_B_VVH01_ELEV_DELAY                      128
#define VR_B_VVH01_SPARE_SPECIAL                   129
#define VR_B_VVH01_RUD_SPECIAL                     130
#define VR_B_VVH01_AIL_SPECIAL                     131
#define VR_B_VVH01_ELEV_SPECIAL                    132
#define VR_B_VVH01_SPARE_SURF_TRIM                 133
#define VR_B_VVH01_RUD_SURF_TRIM                   134
#define VR_B_VVH01_AIL_SURF_TRIM                   135
#define VR_B_VVH01_ELEV_SURF_TRIM                  136
#define VR_B_VVH01_SPARE_POS_TRIM                  137
#define VR_B_VVH01_RUD_POS_TRIM                    138
#define VR_B_VVH01_AIL_POS_TRIM                    139
#define VR_B_VVH01_ELEV_POS_TRIM                   140
#define VR_B_VVH01_SPARE_FORCE_TRIM                141
#define VR_B_VVH01_RUD_FORCE_TRIM                  142
#define VR_B_VVH01_AIL_FORCE_TRIM                  143
#define VR_B_VVH01_ELEV_FORCE_TRIM                 144
#define VR_B_INPUT_FIRST                          145
#define VR_B_VVH01_FORE_DELTA_SWITCH               145
#define VR_B_VVH01_AFT_DELTA_SWITCH                146
#define VR_B_VVH01_AFT_TOTAL_SWITCH                147
#define VR_B_INDEX_MAX                             148

/* "enumeration" of VR_I_INDEX_TYPE */
#define VR_I_INDEX_FIRST                           0
#define VR_I_VHV01_FORE_ELEVATOR_FORCE_TENS_DIGIT  0
#define VR_I_VHV01_FORE_ELEVATOR_FORCE_ONES_DIGIT  1
#define VR_I_VHV01_FORE_ELEVATOR_FORCE_TENTHS_DIGIT 2
#define VR_I_VHV01_FORE_AILERON_FORCE_TENS_DIGIT   3
#define VR_I_VHV01_FORE_AILERON_FORCE_ONES_DIGIT   4
#define VR_I_VHV01_FORE_AILERON_FORCE_TENTHS_DIGIT 5
#define VR_I_VHV01_FORE_RUDDER_FORCE_TENS_DIGIT    6
#define VR_I_VHV01_FORE_RUDDER_FORCE_ONES_DIGIT    7
#define VR_I_VHV01_FORE_RUDDER_FORCE_TENTHS_DIGIT  8
#define VR_I_VHV01_AFT_ELEVATOR_FORCE_TENS_DIGIT   9
#define VR_I_VHV01_AFT_ELEVATOR_FORCE_ONES_DIGIT   10
#define VR_I_VHV01_AFT_ELEVATOR_FORCE_TENTHS_DIGIT 11
#define VR_I_VHV01_AFT_AILERON_FORCE_TENS_DIGIT    12
#define VR_I_VHV01_AFT_AILERON_FORCE_ONES_DIGIT    13
#define VR_I_VHV01_AFT_AILERON_FORCE_TENTHS_DIGIT  14
#define VR_I_VHV01_AFT_RUDDER_FORCE_TENS_DIGIT     15
#define VR_I_VHV01_AFT_RUDDER_FORCE_ONES_DIGIT     16
#define VR_I_VHV01_AFT_RUDDER_FORCE_TENTHS_DIGIT   17
#define VR_I_VHV01_FORE_ELEVATOR_POSITION_ONES_DIGIT 18
#define VR_I_VHV01_FORE_ELEVATOR_POSITION_TENTHS_DIGIT 19
#define VR_I_VHV01_FORE_AILERON_POSITION_ONES_DIGIT 20
#define VR_I_VHV01_FORE_AILERON_POSITION_TENTHS_DIGIT 21
#define VR_I_VHV01_FORE_RUDDER_POSITION_ONES_DIGIT 22
#define VR_I_VHV01_FORE_RUDDER_POSITION_TENTHS_DIGIT 23
#define VR_I_VHV01_AFT_ELEVATOR_POSITION_ONES_DIGIT 24
#define VR_I_VHV01_AFT_ELEVATOR_POSITION_TENTHS_DIGIT 25
#define VR_I_VHV01_AFT_AILERON_POSITION_ONES_DIGIT 26
#define VR_I_VHV01_AFT_AILERON_POSITION_TENTHS_DIGIT 27
#define VR_I_VHV01_AFT_RUDDER_POSITION_ONES_DIGIT  28
#define VR_I_VHV01_AFT_RUDDER_POSITION_TENTHS_DIGIT 29
#define VR_I_VHV01_FORE_ALPHA_METER                30
#define VR_I_VHV01_FORE_NZ_METER                   31
#define VR_I_VHV01_AFT_ALPHA_METER                 32
#define VR_I_VHV01_AFT_NZ_METER                    33
#define VR_I_VHV01_BETA_METER                      34
#define VR_I_VHV01_SPARE_DA1                       35
#define VR_I_VHV01_SPARE_DA2                       36
#define VR_I_VHV01_SPARE_DA3                       37
#define VR_I_VHV01_SPARE_DA4                       38
#define VR_I_VHV01_SPARE_DA5                       39
#define VR_I_VHV01_SPARE_DA6                       40
#define VR_I_VHV01_SPARE_DA7                       41
#define VR_I_VHV01_SPARE_DA8                       42
#define VR_I_VHV01_SPARE_DA9                       43
#define VR_I_VHV01_SPARE_DA10                      44
#define VR_I_VHV01_SPARE_DA11                      45
#define VR_I_VHV01_RECORD_NUMBER_TENS              46
#define VR_I_VHV01_RECORD_NUMBER_ONES              47
#define VR_I_INPUT_FIRST                           48
#define VR_I_VVH01_RECORD_NUMBER_TENS              48
#define VR_I_VVH01_RECORD_NUMBER_ONES              49
#define VR_I_VTH03_TRIM_BUTTONS                    50
#define VR_I_INDEX_MAX                             51
#endif

#ifdef THERE_ARE_IN_DISCRETE_REPOSITORY
/*-- Discrete Outputs from VSS --*/
#define DR_VSSO_FIRST                         0
#define DR_VSSO_SENSOR_TEST_CH1               0
#define DR_VSSO_SURFACE_SENSOR_TEST           1
#define DR_VSSO_TORQUE_UPPER_CG_1             2
#define DR_VSSO_TORQUE_LOWER_CG_1             3
#define DR_VSSO_TORQUE_PILOT_1                4
#define DR_VSSO_TORQUE_RATE_1                 5
#define DR_VSSO_HAPPY_SIGNAL_1                6
#define DR_VSSO_HAPPY_SIGNAL_2                7
#define DR_VSSO_DISENGAGE_TO_ELIC             8
#define DR_VSSO_VSS_DOGFIGHT                  9
#define DR_VSSO_VSS_MISSILE_OVERRIDE          10
#define DR_VSSO_VSS_MISSILE_STEP              11
#define DR_VSSO_LIGHT_1                       12
#define DR_VSSO_LIGHT_2                       13
#define DR_VSSO_LIGHT_3                       14
#define DR_VSSO_LIGHT_4                       15
#define DR_VSSO_LIGHT_5                       16
#define DR_VSSO_LIGHT_6                       17
#define DR_VSSO_AMUX_READY                    18
#define DR_VSSO_THR_SET_LIGHT_FORE_VERY_HIGH  19
#define DR_VSSO_THR_SET_LIGHT_FORE_HIGH       20
#define DR_VSSO_THR_SET_LIGHT_FORE_RIGHT_ON   21
#define DR_VSSO_THR_SET_LIGHT_FORE_LOW        22
#define DR_VSSO_THR_SET_LIGHT_FORE_VERY_LOW   23
#define DR_VSSO_THR_SET_LIGHT_AFT_HIGH        24
#define DR_VSSO_THR_SET_LIGHT_AFT_RIGHT_ON    25
#define DR_VSSO_THR_SET_LIGHT_AFT_LOW         26
#define DR_VSSO_AFTERBURNER_ALLOWED           27
#define DR_VSSO_TEST_POLAR_1                  28
#define DR_VSSO_DMUX_READY                    29
#define DR_VSSO_DFLCC_FMUX_ENABLE             30
#define DR_VSSO_ELIC_SAFETY_TRIP_RESET_1      31
#define DR_VSSO_CHANNEL_TEST_CS_PITCH         32
#define DR_VSSO_CHANNEL_TEST_CS_ROLL          33
#define DR_VSSO_CHANNEL_TEST_SS_PITCH         34
#define DR_VSSO_CHANNEL_TEST_SS_ROLL          35
#define DR_VSSO_CHANNEL_TEST_THROTTLE         36
#define DR_VSSO_CHANNEL_TEST_RUDDER           37
#define DR_VSSO_VSS_READY_LITE_F              38
#define DR_VSSO_VSS_MASTER_CAUTION            39
#define DR_VSSO_VSS_READY_LITE_R              40
#define DR_VSSO_HEUSU                         41
#define DR_VSSO_VOICE_PTT                     42
#define DR_VSSO_SPARE44                       43
#define DR_VSSO_SPARE45                       44
#define DR_VSSO_SPARE46                       45
#define DR_VSSO_SPARE47                       46
#define DR_VSSO_REAR_THROTTLE_CHAN_TEST       47
#define DR_VSSO_SPARE49                       48
#define DR_VSSO_SPARE50                       49
#define DR_VSSO_SPARE51                       50
#define DR_VSSO_SPARE52                       51
#define DR_VSSO_CPC_READY_LITE                52
#define DR_VSSO_LAST                          52
/*-- Discrete Inputs to VSS --*/
#define DR_VSSI_FIRST                         53
#define DR_VSSI_IC_SELECT_FROM_BUTTON         53
#define DR_VSSI_GROUND_SIM_FROM_BUTTON        54
#define DR_VSSI_SAFETY_TRIP_IDENT1_1          55
#define DR_VSSI_SAFETY_TRIP_IDENT1_2          56
#define DR_VSSI_SAFETY_TRIP_IDENT1_3          57
#define DR_VSSI_SAFETY_TRIP_IDENT1_4          58
#define DR_VSSI_SAFETY_TRIP_IDENT1_5          59
#define DR_VSSI_GROUND_SIM_LITE_FROM_DFLCC    60
#define DR_VSSI_FWD_DOGFIGHT                  61
#define DR_VSSI_FWD_MISSILE_OVERRIDE          62
#define DR_VSSI_SCC_1_POWER_FAIL              63
#define DR_VSSI_CS_TMS_RIGHT                  64
#define DR_VSSI_ENGAGE_FROM_BUTTON            65
#define DR_VSSI_SWITCH1                       66
#define DR_VSSI_SWITCH2                       67
#define DR_VSSI_SWITCH3                       68
#define DR_VSSI_SWITCH4                       69
#define DR_VSSI_SWITCH5                       70
#define DR_VSSI_SWITCH6                       71
#define DR_VSSI_AFT_MSL_STEP                  72
#define DR_VSSI_FWD_MSL_STEP                  73
#define DR_VSSI_THROTTLE_SELECTED             74
#define DR_VSSI_FWD_DS_UP                     75
#define DR_VSSI_AFT_DS_UP                     76
#define DR_VSSI_FWD_AFT_DS_DOWN               77
#define DR_VSSI_FWD_AFT_DS_LEFT               78
#define DR_VSSI_FWD_AFT_DS_RIGHT              79
#define DR_VSSI_FWD_AFT_TMS_UP                80
#define DR_VSSI_FWD_AFT_TMS_DOWN              81
#define DR_VSSI_FWD_AFT_TMS_LEFT              82
#define DR_VSSI_FWD_AFT_TMS_RIGHT             83
#define DR_VSSI_FWD_AFT_PINKY_SWITCH          84
#define DR_VSSI_FWD_AFT_CURSOR_ENABLE         85
#define DR_VSSI_AFT_DOGFIGHT                  86
#define DR_VSSI_AFT_MISSILE_OVERRIDE          87
#define DR_VSSI_SS_TRIM_NOSE_DOWN             88
#define DR_VSSI_SS_TRIM_RIGHT_WING_UP         89
#define DR_VSSI_SS_TRIM_NOSE_UP               90
#define DR_VSSI_SS_TRIM_RIGHT_WING_DOWN       91
#define DR_VSSI_THROTTLE_SERVO_ENGAGE         92
#define DR_VSSI_SPARE41                       93
#define DR_VSSI_SPARE42                       94
#define DR_VSSI_SPARE43                       95
#define DR_VSSI_SPARE44                       96
#define DR_VSSI_IDEEC_RDY                     97
#define DR_VSSI_SPARE46                       98
#define DR_VSSI_SPARE47                       99
#define DR_VSSI_SPARE48                       100
#define DR_VSSI_SPARE49                       101
#define DR_VSSI_SPARE50                       102
#define DR_VSSI_SPARE51                       103
#define DR_VSSI_SPARE52                       104
#define DR_VSSI_SPARE53                       105
#define DR_VSSI_CS_TMS_DOWN                   106
#define DR_VSSI_SPARE55                       107
#define DR_VSSI_SPARE56                       108
#define DR_VSSI_SPARE57                       109
#define DR_VSSI_SPARE58                       110
#define DR_VSSI_SPARE59                       111
#define DR_VSSI_SPARE60                       112
#define DR_VSSI_ENGAGE_LITE_FROM_DLFCC        113
#define DR_VSSI_SPARE62                       114
#define DR_VSSI_CS_TMS_UP                     115
#define DR_VSSI_CS_TMS_LEFT                   116
#define DR_VSSI_LAST                          116
/*-- Other discretes not used by VSS --*/
#define DR_GSC_FIRST                          117
#define DR_IC_SELECT_LT                       117
#define DR_SAFETY_TRIP_RESET                  118
#define DR_DISENG_LT                          119
#define DR_VSS_SS_REQ                         120
#define DR_VSS_CS_REQ                         121
#define DR_VSS_SS_ENG_LT                      122
#define DR_VSS_CS_ENG_LT                      123
#define DR_RCP_PNL_DISENG                     124
#define DR_DFLCC_READY_LT                     125
#define DR_INDEX_MAX                          126
#endif
/* CPC Floats */

#define CPC_ALPHA_FORE                          0
#define CPC_ALPHA_AFT                           1
#define CPC_NZ_FORE                             2
#define CPC_NZ_AFT                              3
#define CPC_BETA                                4

/* Input Message Groups */

#define DISCRETE_GROUPING                       0
#define MFD_GROUPING                            1
#define IKP_GROUPING                            2
#define CPC_GROUPING                            3
#define PILOT_CTRL_GROUPING                     4
#define SIM_CTRL_GROUPING                       5
#define LEAR_DISCRETE_GROUPING                 20
#define LEAR_PILOT_CTRL_GROUPING               21

/* Simulation Control Group Indexes */

#define IM_IND_HS_INIT_COND                     0
#define IM_IND_HS_DELTA_TIME                    1
#define IM_IND_HS_STATE_REQ                     2

#define TRIM_SIMULATION                         0
#define TRIM_AND_START_SIMULATION               1
#define START_SIMULATION                        2
#define STOP_SIMULATION                         3

/* Pilot Inceptor Group Indexes */

#define IM_PCG_DF_THROTTLE                      0
#define IM_PCG_VSS_RUDDER_TRIM                  1
#define IM_PCG_FORE_SS_FORCE_PITCH              2
#define IM_PCG_FORE_SS_FORCE_ROLL               3
#define IM_PCG_FORE_SS_DISP_PITCH               4
#define IM_PCG_FORE_SS_DISP_ROLL                5
#define IM_PCG_FORE_CS_FORCE_PITCH              6
#define IM_PCG_FORE_CS_FORCE_ROLL               7
#define IM_PCG_FORE_CS_DISP_PITCH               8
#define IM_PCG_FORE_CS_DISP_ROLL                9
#define IM_PCG_FORE_RUDDER_FORCE               10
#define IM_PCG_TRIM_BUTTONS                    11

/* Lear Discrete Signals Group Indexes */

#define IM_LDSG_FEEL_ENGAGE                     0
#define IM_LDSG_PRESS_ENGAGE                    1
#define IM_LDSG_SYSTEM_ENGAGE                   2
#define IM_LDSG_SAFETY_TRIP_RESET               3
#define IM_LDSG_CS_WC_BUTTON_1                  4
#define IM_LDSG_CS_WC_BUTTON_2                  5
#define IM_LDSG_CS_WC_BUTTON_3                  6
#define IM_LDSG_CS_WC_BUTTON_4                  7
#define IM_LDSG_SS_BUTTON_1                     8
#define IM_LDSG_SS_BUTTON_2                     9
#define IM_LDSG_SS_BUTTON_3                    10
#define IM_LDSG_SS_BUTTON_4                    11
#define IM_LDSG_SP_FIVE_AXIS_UP                12
#define IM_LDSG_SP_FIVE_AXIS_DOWN              13
#define IM_LDSG_SP_FIVE_AXIS_LEFT              14
#define IM_LDSG_SP_FIVE_AXIS_RIGHT             15
#define IM_LDSG_SP_FIVE_AXIS_PUSH              16
#define IM_LDSG_CS_WC_TRIM_NOSE_UP             17
#define IM_LDSG_CS_WC_TRIM_NOSE_DOWN           18
#define IM_LDSG_CS_WC_TRIM_RIGHT_WING_DOWN     19
#define IM_LDSG_CS_WC_TRIM_LEFT_WING_DOWN      20
#define IM_LDSG_SS_TRIM_NOSE_UP                21
#define IM_LDSG_SS_TRIM_NOSE_DOWN              22
#define IM_LDSG_SS_TRIM_RIGHT_WING_DOWN        23
#define IM_LDSG_SS_TRIM_LEFT_WING_DOWN         24
#define IM_LDSG_YAW_TRIM_NOSE_RIGHT            25
#define IM_LDSG_YAW_TRIM_NOSE_LEFT             26
#define IM_LDSG_CS_INSTALLED                   27
#define IM_LDSG_WC_INSTALLED                   28
#define IM_LDSG_SS_INSTALLED                   29
#define IM_LDSG_SS_SELECTED                    30

/* Lear Pilot Control Inceptor Group Indexes */

#define IM_LPCG_CS_PITCH_FORCE                  0
#define IM_LPCG_CS_PITCH_POSITION               1
#define IM_LPCG_CS_ROLL_FORCE                   2
#define IM_LPCG_CS_ROLL_POSITION                3
#define IM_LPCG_WC_PITCH_FORCE                  4
#define IM_LPCG_WC_PITCH_POSITION               5
#define IM_LPCG_WC_ROLL_FORCE                   6
#define IM_LPCG_WC_ROLL_POSITION                7
#define IM_LPCG_SS_PITCH_FORCE                  8
#define IM_LPCG_SS_PITCH_POSITION               9
#define IM_LPCG_SS_ROLL_FORCE                  10
#define IM_LPCG_SS_ROLL_POSITION               11
#define IM_LPCG_RP_FORCE                       12
#define IM_LPCG_RP_POSITION                    13
#define IM_LPCG_LEFT_THROTTLE_POSITION         14
#define IM_LPCG_RIGHT_THROTTLE_POSITON         15

/* Trim Buttons */

#define TRIM_NONE                               0
#define TRIM_NOSE_UP                            1
#define TRIM_NOSE_DOWN                          2
#define TRIM_LEFT_WING_DOWN                     3
#define TRIM_RIGHT_WING_DOWN                    4
#define TRIM_NOSE_LEFT                          5
#define TRIM_NOSE_RIGHT                         6

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* RPI_COMMON_H */                   
