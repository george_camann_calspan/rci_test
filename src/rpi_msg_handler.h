#include "rpi_common.h"
#include "cal_types.h"

#ifndef RPI_MSG_HANDLER
#define RPI_MSG_HANDLER

/*-- CPP COMPLIANT: ---------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* call before using this unit */
void rpi_msg_setup();

/* return supported message version based on msg id */
unsigned char rpi_msg_ver( unsigned char msg_id );

/* return message size based on msg id */
unsigned short rpi_msg_size( unsigned char msg_id );

/* populate a structure pointed to by *data */
STATUS rpi_pack_message( int msg_id, char * data );

/* process an input message */
int rpi_process_input( INPUT_MSG_DATA* input, int param_count );

void rpi_setup_buffer( double * model_data_buffer, double * feel_data_buffer,
					   double * throttle_data_buffer, double * discrete_data_buffer,
					   double * sp_buttons_data_buffer, double * system_data_buffer );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* RPI_MSG_HANDLER */
