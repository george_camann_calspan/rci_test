#ifndef SNS_MSG_HND_C_H
#define SNS_MSG_HND_C_H

/*-- INCLUDES: --------------------------------------------------------------*/

#include "rpi_common.h"
#include "cal_types.h"
#include "sys/types.h"

/*-- CPP COMPLIANT: ---------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Update Rate */
#define SNS_UPDATE_RATE 200

/*-- FUNCTION DECLARATIONS: -------------------------------------------------*/
typedef unsigned char (*SNS_MSG_VER_CALLBACK_TYPE)( unsigned char );

typedef unsigned short (*SNS_MSG_SIZE_CALLBACK_TYPE)( unsigned char );

typedef STATUS (*SNS_PACK_MESSAGE_CALLBACK_TYPE)
    ( int msg_id, char* data );
    
typedef int (*SNS_PROCESS_INPUT_CALLBACK_TYPE)
    ( INPUT_MSG_DATA input[], int param_count );

/* Setup callbacks in unit, do this before sns_msg_init() */    
void sns_msg_setup(SNS_MSG_VER_CALLBACK_TYPE  msg_var_cb,       
    SNS_MSG_SIZE_CALLBACK_TYPE         msg_size_cb,
    SNS_PACK_MESSAGE_CALLBACK_TYPE     pack_msg_cb,
    SNS_PROCESS_INPUT_CALLBACK_TYPE    process_input_cb);
    
/** Initialize the SNS, opens socket.
 *
 * \param inet_addr - zero for default, else specify the INET interface 
 *                    to use in network byte order.
 */    
STATUS          sns_msg_init( u_long inet_addr );
STATUS          sns_process_message( const SNS_MSG * msg );
void            sns_process_messages();
unsigned char   sns_msg_ver( unsigned char msg_id );
unsigned short  sns_msg_size( unsigned char msg_id );
int             sns_process_input( INPUT_MSG_DATA input[], int param_count );
void            sns_list_print();

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* SNS_MSG_HND_C_H */
