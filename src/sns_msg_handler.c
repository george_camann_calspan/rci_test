#include "cal_crc32.h"
#include "cal_platform.h"
#include "cal_types.h"
#include "cal_constants.h"
#include "cal_swap.h"

#include "sns_msg_handler.h"

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/*-- STRUCTURES: ------------------------------------------------------------*/

typedef struct MessageRequestListNode {
    void *      next;               /* 00 Pointer to next item in list. */
    void *      prev;               /* 04 Pointer to prev item in list. */    
    int         remaining_ticks;    /* 08 Timer ticks counter */
    MSG_REQ_MSG mrm;                /* 0C Message Object */
} MSG_REQUEST;                      /* 26 - Total Size */

/* static */
static STATUS sns_pack_message( int msg_id, char * data );

/* Message Handler Variables */

static MSG_REQUEST *  sns_list;       /* LstLib Linked List for Messages */
static CAL_SEM_ID_T   sns_list_sem;   /* Message List Semaphore */

/* ip address to use, AF_INET for default */
static u_long smh_inet_addr = AF_INET;

static SNS_MSG_VER_CALLBACK_TYPE       local_msg_var_cb = NULL;
static SNS_MSG_SIZE_CALLBACK_TYPE      local_msg_size_cb = NULL;
static SNS_PACK_MESSAGE_CALLBACK_TYPE  local_pack_msg_cb = NULL;
static SNS_PROCESS_INPUT_CALLBACK_TYPE local_process_input_cb = NULL;
static int sFd = ERROR;

/* Input Storage Variables */

unsigned char * input_msg = 0;

STATUS sns_send_mrm_response( MSG_REQ_MSG_DATA * request_msg );

void sns_msg_setup(SNS_MSG_VER_CALLBACK_TYPE  msg_var_cb,       
    SNS_MSG_SIZE_CALLBACK_TYPE         msg_size_cb,
    SNS_PACK_MESSAGE_CALLBACK_TYPE     pack_msg_cb,
    SNS_PROCESS_INPUT_CALLBACK_TYPE    process_input_cb)
{
    local_msg_var_cb = msg_var_cb;
    local_msg_size_cb = msg_size_cb;
    local_pack_msg_cb = pack_msg_cb;
    local_process_input_cb = process_input_cb;
}        

/* sns_msg_init
 * 
 * Initializes Message List, Message List Semaphore, MFD module and DECIS module
 */
STATUS sns_msg_init( u_long inet_addr )
{
    cal_sem_create(&sns_list_sem, CAL_SEM_FULL, 0);

    smh_inet_addr = inet_addr;

    /* Build socket */
    if ((sFd = socket (AF_INET, SOCK_DGRAM, 0)) == ERROR) 
    { 
        perror ("socket"); 
        return (ERROR); 
    } 
    
    return OK;
}

/* sns_add_message
 * 
 * Checks for duplicate/update messages. Adds new message to list.
 *
 * param: msg_req   pointer to message request to add to list
 */

void sns_add_message( MSG_REQUEST * msg_req )
{
    MSG_REQUEST * current = sns_list;
    
    cal_sem_take( sns_list_sem, CAL_WAIT_FOREVER, 0 );
    
    if( sns_list )
    {        
        /* check for duplicate or updated message */
        
        do
        {
            /* check for identical IP, Port and Message ID */
            
            if( (current->mrm.data.ip_addr == msg_req->mrm.data.ip_addr)
             && (current->mrm.data.port == msg_req->mrm.data.port)
             && (current->mrm.data.msg_id == msg_req->mrm.data.msg_id) )
            {
                /* check rate and duration for match */
                
                if( (current->mrm.data.msg_rate != msg_req->mrm.data.msg_rate)
                 || (current->mrm.data.msg_duration 
                     != msg_req->mrm.data.msg_duration) )
                {
                    /* Update message */
                    
                    current->mrm.data.msg_rate = msg_req->mrm.data.msg_rate;
                        
                    current->mrm.data.msg_duration = 
                        msg_req->mrm.data.msg_duration;
                        
                    current->remaining_ticks = msg_req->remaining_ticks;
                }
                
                /* Message has either been used to update a message in the  */
                /* list or is a duplicate. Either way, we're done with it   */
                /* and can free the memory.                                 */
                
                free( msg_req );
               
                cal_sem_give( sns_list_sem );
                return;                
            }         
            current = current->next;
        } while( current );
    }

    /* New message, add to list */
 
    
    if( (NULL != sns_list) && (NULL == current) )
    {
        current = sns_list;
        while( NULL != current->next )
        {
            current = current->next;
        }
        current->next = msg_req;
        msg_req->prev = current;
    }
    else
    {
        sns_list = msg_req;
    }
    //sns_list_print();
    
    cal_sem_give( sns_list_sem );
};

/* sns_process_message
 * 
 * Processes incoming message
 *
 * param: msg   Pointer to incoming message structure.
 * return: status of operation
 */

STATUS sns_process_message( const SNS_MSG* msg )
{
    MSG_REQUEST * msg_req = NULL;
    unsigned int checksum = 0;
    int param_count = 0;
    MSG_HDR hdr;
    
    /* Get a copy of the header in correct byte order */
    hdr.checksum = cal_ntohl( msg->header.checksum );
    hdr.msg_id   = msg->header.msg_id;
    hdr.msg_ver  = msg->header.msg_ver;
    hdr.msg_size = cal_ntohs( msg->header.msg_size );
    hdr.msg_time_msec = cal_ntohl( msg->header.msg_time_msec );
    hdr.reserved = cal_ntohl( msg->header.reserved );

    /*** Check header ***/

    /* Check for valid checksum */
    
    checksum = crc32_full( (unsigned char *) &msg->header.msg_size, hdr.msg_size - 4 );

    if( hdr.checksum != checksum )
    {
        printf( "Error: Invalid message checksum received.\n" );
        printf( "Received: %x Generated: %x\n", hdr.checksum, checksum );
        return( ERROR );
    }

    /* Check for valid id, version and size */
    
    if( (hdr.msg_id > MSG_ID_MAX) )
    {
        printf( "Error: Invalid message id received.\n" );
        printf( "Received: %i Expected: 0-%i\n", hdr.msg_id, MSG_ID_MAX );
        return( ERROR );
    }
    
    if( hdr.msg_ver != sns_msg_ver( hdr.msg_id ) )
    {
        printf( "Error: Invalid message version received.\n" );
        printf( "Received: %i Expected: %i\n", hdr.msg_ver, sns_msg_ver( hdr.msg_id ) );
        return( ERROR );
    }
	
	if ( (hdr.msg_size != sns_msg_size( hdr.msg_id ))
	  && (hdr.msg_id   != MSG_ID_IM) )
	{
		printf("Error: Invalid message size detected for message type.\n" );
        printf("Received: %i Expected: %i\n", hdr.msg_size, sns_msg_size( hdr.msg_id ) );
		return( ERROR );
	}
    else if ( (hdr.msg_id == MSG_ID_IM)
              && ( (hdr.msg_size < sns_msg_size( hdr.msg_id ))
		        || (hdr.msg_size >= sns_msg_size( hdr.msg_id ) + 31 * sizeof(INPUT_MSG_DATA) ) ) )
    {
        printf("Error: Invalid message size detected for message type.\n" );
        printf("Received: %i Expected: %i-%i\n", hdr.msg_size, sns_msg_size( hdr.msg_id ), (int) (sns_msg_size( hdr.msg_id ) + 31 * sizeof(INPUT_MSG_DATA)) );
		return( ERROR );
    }
    
    /* Switch on message id type */
    
    switch( hdr.msg_id )
    {
        case MSG_ID_MRM: /* SNS_MSG REQ SNS_MSG */
            msg_req = (MSG_REQUEST *) malloc( sizeof(MSG_REQUEST) );
            memset( msg_req, 0, sizeof(MSG_REQUEST) );
            memcpy( &msg_req->mrm, msg, sizeof(MSG_REQ_MSG) );
            
            /* Setup message time counter */
            
            if( msg->data.msg_req_msg.msg_duration == 255 )
            {
                msg_req->remaining_ticks = 
                    pow( 2, msg->data.msg_req_msg.msg_rate ) - 1;
            }
            else if( msg->data.msg_req_msg.msg_duration == 0 )
            {
                msg_req->remaining_ticks = 0;
            }
            else
            {
                msg_req->remaining_ticks = 
                        SNS_UPDATE_RATE 
                        * msg->data.msg_req_msg.msg_duration - 1;
            }

            /* Swap */
            msg_req->mrm.data.ip_addr = cal_ntohl( msg_req->mrm.data.ip_addr );
            msg_req->mrm.data.port = cal_ntohs( msg_req->mrm.data.port );

            sns_add_message( msg_req );
			break;
            
        case MSG_ID_IM:

            /* Handle Input Message */

            param_count = (hdr.msg_size - sizeof(MSG_HDR)) / sizeof(INPUT_MSG_DATA);
            
            if( (param_count <= 0) || (param_count > 32) )
            {
                printf( "Error: Invalid number of input parameters received.\n" );
                return( ERROR );
            }
            
            input_msg = (unsigned char *) malloc( sizeof(INPUT_MSG_DATA) * param_count );
            memcpy( input_msg, msg->data.input_msg, sizeof(INPUT_MSG_DATA) * param_count );
            
            sns_process_input( (INPUT_MSG_DATA*) input_msg, param_count );
            break;
            
        default:
            printf( "Error: Invalid message id received.\n" );
            return( ERROR );
    }
    
    return( OK );
};

/* sns_process_messages
 * 
 * Processes message list
 */

void sns_process_messages()
{
    cal_sem_take( sns_list_sem, CAL_WAIT_FOREVER, 0 );
    
    if( sns_list )
    {
        MSG_REQUEST * current = sns_list;
        MSG_REQUEST * del_node;
        
        do
        {
            /* process current message request */
            
            if( current->remaining_ticks 
                % (int) pow( 2, current->mrm.data.msg_rate ) == 0)
            {
            	if( sns_send_mrm_response( &current->mrm.data ) == ERROR )
				{
					/* Error in sending, remove message entry */
					
					current->mrm.data.msg_duration = 0;
					current->remaining_ticks = 0;
				}
                
                if( (current->mrm.data.msg_duration == 255)
                 && (current->remaining_ticks == 0))
                {
                    /* Continuous message send case, give more time */
                    
                    current->remaining_ticks = 
                        pow( 2, current->mrm.data.msg_rate );
                }
            }
            
            if( current->remaining_ticks == 0 )
            {
                /* Non-continuous message, delete if expired */
                
                del_node = current;
                if( current->prev )
                {
                    current = current->prev;
                    if( del_node->next )
                    {
                        current->next = del_node->next;
                        ((MSG_REQUEST *)current->next)->prev = current; 
                    }
                    else
                    {
                        current->next = NULL;
                    }
                }
                else
                {
                    if( del_node->next )
                    {
                        current = del_node->next;
                        current->prev = NULL;
                    }
                    else
                    {
                        current = NULL;
                    }
                    sns_list = current;
                }

                free( (char *) del_node );
            }
            else
            {            
                /* decriment counter */
                
                current->remaining_ticks--;          
            }                
        } while( current && (current = current->next) );
	} 

    cal_sem_give( sns_list_sem );
};

/* sns_send_mrm_response
 * 
 * Processes message request message and builds/sends response
 *
 * param: request_msg   pointer to the message request message to respond to
 * return: status of operation
 */

STATUS sns_send_mrm_response( MSG_REQ_MSG_DATA * request_msg )
{
    SNS_MSG response_msg;
    struct sockaddr_in  clientAddr;    /* clients's socket address */ 
    int                 sockAddrSize;  /* size of socket address structure */ 
    CAL_TIME_T timestamp = cal_getclock();

    memset( &response_msg, 0, sizeof( SNS_MSG ) );
    
    /* Build response message header */
    
    response_msg.header.msg_size = cal_htons( sns_msg_size( request_msg->msg_id ) );
    response_msg.header.msg_id = request_msg->msg_id;
    response_msg.header.msg_ver = request_msg->msg_ver;
    response_msg.header.msg_time_msec = cal_htonl( timestamp.sec * 1000 + timestamp.nsec / 1000000 );
  
    /* Build client socket address */ 
 
    sockAddrSize = sizeof (struct sockaddr_in); 
    bzero ((char *) &clientAddr, sockAddrSize); 
#ifdef __vxworks
    clientAddr.sin_len = (u_char) sockAddrSize; 
#endif
    clientAddr.sin_family = smh_inet_addr; 
    clientAddr.sin_port = cal_htons( request_msg->port );
    clientAddr.sin_addr.s_addr = cal_htonl( request_msg->ip_addr );
    
    /* Pack message data */
    
    if( sns_pack_message( response_msg.header.msg_id, 
                          (char *) &response_msg.data ) == ERROR )
    {
		printf( "Error packing message.\n" );
        return( ERROR );
	}
    
    /* Checksum */
    
    response_msg.header.checksum = 
            crc32_full( (unsigned char *) &response_msg.header.msg_size, 
            			sns_msg_size( request_msg->msg_id ) - 4 );
    response_msg.header.checksum = cal_htonl( response_msg.header.checksum );

    /* Send response to client */ 
 
    if ( sendto (sFd, (caddr_t) &response_msg, sns_msg_size( request_msg->msg_id ), 0,
                    (struct sockaddr *) &clientAddr, sockAddrSize) == ERROR ) 
    { 
        perror( "sendto" ); 
		printf( "Trying to send to IP address %i.%i.%i.%i and port %i\n", 
				*(((char *) &request_msg->ip_addr)+0),
				*(((char *) &request_msg->ip_addr)+1),
				*(((char *) &request_msg->ip_addr)+2),
				*(((char *) &request_msg->ip_addr)+3),		
				request_msg->port );
		printf( "Could not reach IP address supplied in MRM. Removing client from queue.\n" );
        close( sFd ); 
        return( ERROR ); 
    } 
    
    return( OK );
}

/* sns_msg_ver
 * 
 * Returns supported message version for a given message id
 *
 * param: msg_id   byte containing message id
 * returns: message version stored as an unsigned byte
 */

unsigned char sns_msg_ver( unsigned char msg_id )
{
    if( NULL != local_msg_var_cb )
        return (*local_msg_var_cb)(msg_id);
    else
        return 0;
}

/* sns_msg_size
 * 
 * Returns supported message size for a given message id
 *
 * param: msg_id   byte containing message id
 * returns: message size stored as an unsigned short
 */

unsigned short sns_msg_size( unsigned char msg_id )
{
    if( NULL != local_msg_size_cb )
        return (*local_msg_size_cb)(msg_id);
    else
        return 0;
}

/* sns_pack_message
 * 
 * packs data according to message id for later submission to client
 *
 * param: msg_id    int containing message id
 *        data      pointer to data
 * returns: status of data packing
 */

static STATUS sns_pack_message( int msg_id, char * data )
{
    if( NULL != local_pack_msg_cb )
        return (*local_pack_msg_cb)(msg_id, data);
    else
        return ERROR;
}

int sns_process_input( INPUT_MSG_DATA input[], int param_count )
{
    if( NULL != local_process_input_cb )
        return (*local_process_input_cb)(input, param_count);
    else
        return ERROR;

    return(0);
}

void sns_list_print()
{
    int index = 0;
    
    //cal_sem_take( sns_list_sem, CAL_WAIT_FOREVER, 0 );
    
    printf( "local_msg_var_cb  = %p\n",  local_msg_var_cb);
    printf( "local_msg_size_cb = %p\n",  local_msg_size_cb);
    printf( "local_pack_msg_cb = %p\n",  local_pack_msg_cb);
    printf( "local_process_input_cb = %p\n",  local_process_input_cb);

    if( sns_list )
    {
        MSG_REQUEST * current = sns_list;
        
        /* check for duplicate or updated message */
        
        do
        {
            printf( "========================= %i@%p\n", 
                index, current);
            printf( "ip_addr  = %#10x\n", current->mrm.data.ip_addr );
            printf( "port     = %#06x %i\n", 
                current->mrm.data.port, current->mrm.data.port);
            printf( "msg_id   = %#06x %i\n",  
                current->mrm.data.msg_id, current->mrm.data.msg_id);
            printf( "msg_rate = %#06x %i\n", 
                current->mrm.data.msg_rate, current->mrm.data.msg_rate);
            printf( "msg_dur  = %#06x %i\n", 
                current->mrm.data.msg_duration, current->mrm.data.msg_duration);
            printf( "remaining_ticks = %i\n", current->remaining_ticks);
            index++;
  
            current = current->next;
        } while( current );
    }

    //cal_sem_give( sns_list_sem );
};
